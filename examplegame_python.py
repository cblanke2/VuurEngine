#!/usr/bin/python3

import vuurengine

title_card = """
=============================================================================||
||                                                                           ||
||     ▄     ▄     ▄   █▄▄▄▄ ▄███▄      ▄     ▄▀  ▄█    ▄   ▄███▄            ||
||      █     █     █  █  ▄▀ █▀   ▀      █  ▄▀    ██     █  █▀   ▀           ||
|| █     █ █   █ █   █ █▀▀▌  ██▄▄    ██   █ █ ▀▄  ██ ██   █ ██▄▄             ||
||  █    █ █   █ █   █ █  █  █▄   ▄▀ █ █  █ █   █ ▐█ █ █  █ █▄   ▄▀          ||
||   █  █  █▄ ▄█ █▄ ▄█   █   ▀███▀   █  █ █  ███   ▐ █  █ █ ▀███▀            ||
||    █▐    ▀▀▀   ▀▀▀   ▀            █   ██          █   ██                  ||
||    ▐                                                                      ||
|| ▄███▄      ▄  ██   █▀▄▀█ █ ▄▄  █     ▄███▄         ▄▀  ██   █▀▄▀█ ▄███▄   ||
|| █▀   ▀ ▀▄   █ █ █  █ █ █ █   █ █     █▀   ▀      ▄▀    █ █  █ █ █ █▀   ▀  ||
|| ██▄▄     █ ▀  █▄▄█ █ ▄ █ █▀▀▀  █     ██▄▄        █ ▀▄  █▄▄█ █ ▄ █ ██▄▄    ||
|| █▄   ▄▀ ▄ █   █  █ █   █ █     ███▄  █▄   ▄▀     █   █ █  █ █   █ █▄   ▄▀ ||
|| ▀███▀  █   ▀▄    █    █   █        ▀ ▀███▀        ███     █    █  ▀███▀   ||
||         ▀       █    ▀     ▀                             █    ▀           ||
||                ▀                                        ▀                 ||
||                                                                           ||
||                    Copyright © 2023, Chris Blankenship                    ||
|| cblankenship@pm.me                               www.chrisblankenship.lol ||
||=============================================================================
"""

# # # #
#
# Print title card
#
print(title_card)
#
# Ask player for name
try:
	player_name = input("What is your name? ")
	print()
except KeyboardInterrupt:
	print()
	print()
	exit()
# Handles "Ctrl+D"
except EOFError:
	print()
	print()
	exit()
#
# Custom additions using conditionals
god_mode = False
if player_name.lower() == "it is 'arthur', king of the britons.":
	try:
		what_quest= input('What... is your quest? ').lower()
		print()
		what_swallow = input('What... is the air-speed velocity of an unladen swallow? ').lower()
		print()
		if (what_quest == 'to seek the holy grail.') and (what_swallow == 'what do you mean? african or european?'):
			god_mode = True 
			player_name = 'Yellow Bastard'
		else:
			player_name = 'Sir Robin'
	except KeyboardInterrupt:
		print()
		print()
		exit()
	# Handles "Ctrl+D"
	except EOFError:
		print()
		print()
		exit()

print("===============================================================================")
print()
#
# # # #

# # # #
#
# Initialize the engine instance
#
# Required Params: Game Name (String), Copyright Year (String), Copyright Holder Name (String), Copyright Holder Email (String), Copyright Holder Website (String)
# Optional Params: Game Contributors Title (String, optional), Game Contributors (Dict, optional), Debug (Bool, optional)
examplegame_engine = vuurengine.VuurEngine("VuurEngine Example Game (Python)", "2023", "Chris Blankenship", "cblankenship@pm.me", "www.chrisblankenship.lol", debug_allowed=False)
# 
# # # #

# # # #
#
# Create the Rooms
#
# Required Params: Engine Instance (VuurEngine), Room Name (String), Locked (Bool), Hidden (Bool)
# Optional Params: Password (String)
examplegame_room_center = vuurengine.Room(examplegame_engine, "Central Room", False, False)
examplegame_room_north = vuurengine.Room(examplegame_engine, "North Room", False, False)
examplegame_room_south = vuurengine.Room(examplegame_engine, "South Room", False, False)
examplegame_room_east = vuurengine.Room(examplegame_engine, "East Room", False, False)
examplegame_room_west = vuurengine.Room(examplegame_engine, "West Room", True, True)
examplegame_room_attic = vuurengine.Room(examplegame_engine, "Attic Room", True, False)
examplegame_room_basement = vuurengine.Room(examplegame_engine, "Basement Room", False, True)
#
# Link the rooms together
#
# Required Params: North (Room or None), South (Room or None), East (Room or None), West (Room or None), Up (Room or None), Down (Room or None)
examplegame_room_center.linkRoom(examplegame_room_north, examplegame_room_south, examplegame_room_east, examplegame_room_west, examplegame_room_attic, examplegame_room_basement)
examplegame_room_north.linkRoom(None, examplegame_room_center, None, None, None, None)
examplegame_room_south.linkRoom(examplegame_room_center, None, None, None, None, None)
examplegame_room_east.linkRoom(None, None, None, examplegame_room_center, None, None)
examplegame_room_west.linkRoom(None, None, examplegame_room_center, None, None, None)
examplegame_room_attic.linkRoom(None, None, None, None, None, examplegame_room_center)
examplegame_room_basement.linkRoom(None, None, None, None, examplegame_room_center, None)
#
# # # #

# # # #
#
# Create the main player
#
# Required Params: Engine Instance (VuurEngine), Player Name (String), Location (Room)
examplegame_player = vuurengine.Player(examplegame_engine, player_name, examplegame_room_center)
# 
# # # #

# # # #
#
# Create non-player mobs
#
# Required Params: Engine Instance (VuurEngine), Mob Name (String), Location (Room), Hostime (Bool)
# Optional Params: Synonyms (List)
examplegame_mob_goblinbill = vuurengine.Mob(examplegame_engine, "Bill The Goblin", examplegame_room_south, True, synonyms=['bill'])
examplegame_mob_goblindave = vuurengine.Mob(examplegame_engine, "Dave The Goblin", examplegame_room_south, True, synonyms=['dave'])
examplegame_mob_goblintom = vuurengine.Mob(examplegame_engine, "Tom The Cool Goblin", examplegame_room_south, False, synonyms=['tom', 'cool goblin', 'goblin dude'])
examplegame_mob_felix = vuurengine.Mob(examplegame_engine, "Felix", examplegame_room_north, False)
examplegame_mob_finalboss = vuurengine.Mob(examplegame_engine, "Gary The Head Goblin", examplegame_room_west, True, synonyms=['gary'])
#
# Add info to mobs
examplegame_mob_goblintom.addTalkingPoint("It sure is dark in here.")
#
# # # #

# # # #
# 
# Create Items
#
# Required Params: Engine Instance (VuurEngine), Item Name (String), Item Description (String), Synonyms (List)
# Optional Params: Synonyms (List)
examplegame_item_atticcode = vuurengine.Item(examplegame_engine, "Attic Room Code", "Some Paper With The Attic Code", synonyms=['attic code'])
examplegame_item_westcode = vuurengine.Item(examplegame_engine, "West Room Code", "Some Paper With The West Room Code", synonyms=['west code'])
#
# Modify Items
#
examplegame_item_atticcode.setWriting(examplegame_room_attic.getInfo('password'))
examplegame_item_westcode.setWriting(examplegame_room_west.getInfo('password'))
#
# Create Consumables
#
# Required Params: Engine Instance (VuurEngine), Consumable Name (String), Consumable Description (String), Attack Boost (Int), Health Boost (Int)
# Optional Params: Synonyms (List)
examplegame_consumable_attackpotion = vuurengine.Consumable(examplegame_engine, "Attack Potion", "Boosts Your Attack", 15, 0)
examplegame_consumable_healthpotion = vuurengine.Consumable(examplegame_engine, "Health Potion", "Boosts Your Health", 0, 15)
examplegame_consumable_poison = vuurengine.Consumable(examplegame_engine, "Poison Potion", "It's Literally Poison", -110, -110, synonyms=['poison'])
#
# Create Weapons
#
# Required Params: Engine Instance (VuurEngine), Weapon Name (String), Weapon Description (String), Attack (Int), 
# Optional Params: Synonyms (List)
examplegame_weapon_rustydagger = vuurengine.Weapon(examplegame_engine, "Rusty Dagger", "Bringer of Tetanus", 5, synonyms=['dagger', 'knife'])
examplegame_weapon_bronzesword = vuurengine.Weapon(examplegame_engine, "Bronze Sword", "50% Tin, 50% Copper, 100% Satisfaction", 8)
examplegame_weapon_ironsword = vuurengine.Weapon(examplegame_engine, "Iron Sword", "More-Or-Less Sharp", 10)
examplegame_weapon_battleaxe = vuurengine.Weapon(examplegame_engine, "Battle Axe", "Monastery Tested, Viking Approved", 0)
examplegame_weapon_runicsword = vuurengine.Weapon(examplegame_engine, "Runic Sword", "1v1 m3 1N Da W1LDyy n00b", 30)
examplegame_weapon_chainsaw = vuurengine.Weapon(examplegame_engine, "Chainsaw", "The Exhaust Fumes Add Ambiance", 50)
examplegame_weapon_excalibur = vuurengine.Weapon(examplegame_engine, "Not Excalibur", "But Just As Waterlogged", 200)
# 
# Store items/consumables/weapons
#
examplegame_room_basement.giveItem(examplegame_consumable_attackpotion)
examplegame_room_basement.giveItem(examplegame_consumable_healthpotion)
examplegame_room_basement.giveItem(examplegame_weapon_battleaxe)
examplegame_room_north.giveItem(examplegame_item_atticcode)
examplegame_room_center.giveItem(examplegame_weapon_rustydagger)
examplegame_room_attic.giveItem(examplegame_weapon_chainsaw)
examplegame_mob_goblindave.equip(examplegame_weapon_bronzesword)
if (god_mode == True):
	examplegame_player.equip(examplegame_weapon_excalibur)
examplegame_mob_felix.giveItem(examplegame_consumable_poison)
# # # #

# # # #
#
# Create Questmaster
# Required Params: Engine Instance (VuurEngine), Questmaster Name (String), Location (Room)
# Optional Params: Synonyms (List)
examplegame_questmaster_thequestmaster = vuurengine.Questmaster(examplegame_engine, "The Questmaster", examplegame_room_east, synonyms=['questmaster', 'questgiver'])
#
# Add info to Questmaster
examplegame_questmaster_thequestmaster.addTalkingPoint("Why did I even come to this dungeon?")
#
# Create the Quests
# Required Params: Engine Instance (VuurEngine), Fetch or Kill (String, "fetch" or "kill"), Target (Item if "fetch", Mob if "kill"), Reward (Item or None), Final Quest (Bool)
examplegame_quest_atticcode = vuurengine.Quest(examplegame_engine, "Lost Keys", "fetch", examplegame_item_atticcode, None, False)
examplegame_quest_blamefelix = vuurengine.Quest(examplegame_engine, "Blame Felix", "kill", examplegame_mob_felix, examplegame_weapon_ironsword, False)
examplegame_quest_poison = vuurengine.Quest(examplegame_engine, "Harm Reduction", "fetch", examplegame_consumable_poison, None, False)
examplegame_quest_goblinslayer = vuurengine.Quest(examplegame_engine, "Goblin Slayer", "kill", examplegame_mob_goblintom, examplegame_item_westcode, False)
examplegame_quest_finally = vuurengine.Quest(examplegame_engine, "Finally", "kill", examplegame_mob_finalboss, None, True)
#
# Add info to Quests
examplegame_quest_atticcode.setInfo("quest_description", "I lost the code to the attic somewhere in this dungeon. Be a pal and find it for me? Thanks.")
examplegame_quest_atticcode.setInfo("quest_completion_text", "Great. The room has things I need.")
#
examplegame_quest_blamefelix.setInfo("quest_description", "The code was in the same room as Felix? That asshole. He probably stole it. Why don't you... take care of him for me?")
examplegame_quest_blamefelix.setInfo("quest_completion_text", "Good. One more down. A few more to go.")
#
examplegame_quest_poison.setInfo("quest_description", "Did Felix have any, well, uh... poison on him? I'm gonna need that. For reasons.")
examplegame_quest_poison.setInfo("quest_completion_text", "Don't worry. One of the 'reasons' isn't you. Unless you went in the attic. Then it is. Sorry.")
#
examplegame_quest_goblinslayer.setInfo("quest_description", "You know what... can you go ahead and take care of Tom too? He's somewhere around here.")
examplegame_quest_goblinslayer.setInfo("quest_completion_text", "And that's what he gets for inviting Felix to his parties, but not me! Anyways, you're gonna need this other code.")
#
examplegame_quest_finally.setInfo("quest_description", "So about that code. I can't leave here until the head goblin is dead. He's in the western room. Chop chop.")
examplegame_quest_finally.setInfo("quest_completion_text", "Haha, get it 'chop chop'? Like, chopping his head off? Get it?")
#
# Add prerequisite Quests
#
# Required Params: Prereq (Quest)
examplegame_quest_blamefelix.addPrereq(examplegame_quest_atticcode)
examplegame_quest_poison.addPrereq(examplegame_quest_blamefelix)
examplegame_quest_goblinslayer.addPrereq(examplegame_quest_blamefelix)
examplegame_quest_finally.addPrereq(examplegame_quest_goblinslayer)
examplegame_quest_finally.addPrereq(examplegame_quest_poison)
#
# Add quests to Questmaster
#
examplegame_questmaster_thequestmaster.addQuestToGive(examplegame_quest_atticcode)
examplegame_questmaster_thequestmaster.addQuestToGive(examplegame_quest_blamefelix)
examplegame_questmaster_thequestmaster.addQuestToGive(examplegame_quest_poison)
examplegame_questmaster_thequestmaster.addQuestToGive(examplegame_quest_goblinslayer)
examplegame_questmaster_thequestmaster.addQuestToGive(examplegame_quest_finally)
#
# # # #

# # # #
#
# Create the game interpreter
# Required Params: Engine Instance (VuurEngine), Main Player (Player)
examplegame_interpreter = vuurengine.Interpreter(examplegame_engine, examplegame_player)
#
# Start the game console
examplegame_interpreter.gameConsole()
#
# # # #

# # # #
# 
# Exits the game
#
exit()
# # # #
