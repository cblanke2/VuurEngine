#!/usr/bin/python3

#  VuurEngine (vuurengine.py)
#
#  Copyright (c) 2023, Chris Blankenship <cblankenship@pm.me>
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import base64, random, re, readline, zlib

version = '0.4.3'
engine_copyright_year = '2023'
engine_copyright_holder = 'Chris Blankenship'
engine_copyright_email = 'cblankenship@pm.me'
engine_copyright_website_string = 'www.chrisblankenship.lol • www.gitlab.com/cblanke2/vuurengine'
engine_other_contributors = {
}

break_interface_loop = False

#  name: VuurEngine
#
#  This provides the basic functions of a VuurEngine instance,
#  such as info on the engine itself, common functions,
#  and so on.
class VuurEngine:
	
	# # # #
	# Public Functions
	# # # #
	
	#  name: VuurEngine.__init__
	#  @param game_title - String
	#  @param game_copyright_year - String
	#  @param game_copyright_holder - String
	#  @param game_copyright_email - String
	#  @param game_copyright_website_string - String
	#  @param game_contributors_title - String or None (optional)
	#  @param game_contributors - Dict or None (optional)
	#  @param debug_allowed - Bool (optional)
	#
	# Initialize the VuurEngine instance
	def __init__(self, game_title, game_copyright_year, game_copyright_holder, game_copyright_email, game_copyright_website_string, **kwargs):
		self.game_contributors_title = kwargs.get('game_contributors_title', "Game Contributors")
		self.game_contributors = kwargs.get('game_contributors', {})
		self.debug_allowed = kwargs.get('debug_allowed', False)
		#
		# Title of the game
		self.game_title = game_title
		#
		# Info on who owns the copyright to the game itself
		self.game_copyright_year = game_copyright_year
		self.game_copyright_holder = game_copyright_holder
		self.game_copyright_email = game_copyright_email
		self.game_copyright_website_string = game_copyright_website_string
		#
		# A list of objects associated with this instance of the engine
		self.object_list = []
		#
		# Adds self to 'object_list' list
		self.registerObject(self)
		#
		# Number of turns
		self.turns = 0
		#
		# Has the game been won?
		self.game_won = False
	
	# # # #
	# Private Functions
	# # # #

	#  name: VuurEngine.registerObject
	#  @param game_title - Object
	#  @return 0 - Int
	#
	# Store all objects in a list
	def registerObject(self, object_to_register):
		#
		# Adds passed object to 'object_list' list
		self.object_list.append(object_to_register)
		#
		return 0

	#  name: VuurEngine.refreshAll
	#  @return 0 - Int
	#
	# Refresh all Mob and Room objects
	def refreshAll(self):
		#
		# Refresh all Rooms and Non-Player Mobs first
		for engine_object in self.object_list:
			if (((isinstance(engine_object, Mob)) and (type(engine_object != Player))) or (isinstance(engine_object, Room))):
				engine_object.refresh()
		#
		# Then refresh Players
		for engine_object in self.object_list:
			if (type(engine_object) == Player):
				engine_object.playerRefresh()
		#
		# Increment turns
		self.turns += 1
		#
		return 0

	#  name: VuurEngine.exitGame
	#  @return 0 - Int
	#
	# Exit the game
	def exitGame(self):
		#
		global break_interface_loop
		break_interface_loop = True
		#
		print("Goodbye!")
		#
		# Deletes all engine objects
		for engine_object in self.object_list:
			if (engine_object != self):
				del engine_object
		#
		return 0
	
	#  name: VuurEngine.gameInfo
	#  @return 0 - Int
	#
	# Get the game information
	def gameInfo(self):
		#
		# Print game specific information
		print(self.game_title)
		print('Copyright © ' + self.game_copyright_year + ', ' + self.game_copyright_holder + ' <' + self.game_copyright_email + '>')
		if ((self.game_copyright_website_string != None) and (self.game_copyright_website_string != "")):
			print(self.game_copyright_website_string)
		#
		# If there are other engine contributors, print their information as well
		if (len(self.game_contributors) > 0):
			print('---')
			print(self.game_contributors_title + ":")
			for contributor in self.game_contributors:
				print(contributor + " - " + self.game_contributors[contributor])
		#
		# Print engine copyright information
		print('---')
		print('Built on the VuurEngine:  v' + version)
		print('Copyright © ' + engine_copyright_year + ', ' + engine_copyright_holder + ' <' + engine_copyright_email + '>')
		print(engine_copyright_website_string)
		#
		# If there are other engine contributors, print their information as well
		if (len(engine_other_contributors) > 0):
			print('---')
			print("Engine Contributors:")
			for contributor in engine_other_contributers:
				print(contributor + " - " + engine_other_contributors[contributor])
			
		return 0
	
	#  name: VuurEngine.gameHelp
	#  @return 0 - Int
	#
	# Get help information on game
	def gameHelp(self):
		print("Valid Commands:")
		print("\tattack [Mob], fight [Mob], kill [Mob]")
		print("\tdrop [Item], toss [Item]")
		print("\tequip [Weapon], wield [Weapon]")
		print("\tequipped")
		print("\texamine [Item], check [Item]")
		print("\texit, quit, close, clear")
		print("\tgive [Item] to [Mob], hand [Item] to [Mob]")
		print("\thealth, stats, turns")
		print("\thelp")
		print("\tinfo")
		print("\tinventory")
		print("\tlook")
		print("\tmove [Direction], go [Direction], [Direction]")
		print("\tquestlog, quests")
		print("\tread [Item]")
		print("\ttake [Item], grab [Item], pick up [Item], get [Item]")
		print("\ttalk with [Mob], talk to [Mob], speak with [Mob], speak to [Mob]")
		print("\tunequip")
		print("\tuse [Consumable], consume [Consumable]")
		return 0

	#  name: VuurEngine.stats
	#  @return 0 - Int
	#
	# Print game stats
	def gameStats(self):
		print("Turns: " + str(self.turns))
		return 0
	
	#  name: VuurEngine.vuurPassword
	#  @param to_password - String
	#  @return password_result - String
	#
	# Create a strong password based on a string
	def vuurPassword(self, to_password):
		password_len = len(to_password)
		password_list = list(to_password)
		password_number = 0
		password_result = ""
		for character in password_list:
			rand_low = 0
			rand_high = ord(character)
			password_number = ((password_number + int(random.randint(rand_low, rand_high))) * password_len)
			password_len -= 1
		password_raw = str(base64.b64encode(zlib.compress(str(password_number).encode(),9)))[2:-1]
		password_result = ''.join(random.sample(password_raw,len(password_raw)))
		return password_result
	
	#  name: VuurEngine.debugConsole
	#  @return 0 - Int
	#
	# Provide a debug console to directly interact with the engine
	def debugConsole(self):
		if (self.debug_allowed == True):
			while True:
				try:
					command = input('>>> ')
					print()
					if (command.startswith("import ")):
						print("Importing more libraries is not allowed...")
					else:
						try:
							eval(command)
						except Exception as e:
							print(e)
				# Handles "Ctrl+C"
				except KeyboardInterrupt:
					print()
					print()
					break
				# Handles "Ctrl+D"
				except EOFError:
					print()
					print()
					break
		else:
			print("Debug Console disabled...")
		return 0

	#  name: VuurEngine.gameWon
	#  @return 0 - Int
	#
	# Sets game_won to True
	def gameWon(self):
		self.game_won = True
		return 0
	
	#  name: VuurEngine.isWon
	#  @return game_won - Bool
	#
	# Returns True or False if a game has been won
	def isWon(self):
		return self.game_won

#  name: Interpreter
#
#  The Interprester provides the interface for the VuurEngine.
#  It parses and executes commands issued by a Player.
class Interpreter:
	
	# # # #
	# Public Functions
	# # # #
	
	#  name: Interpreter.__init__
	#  @param engine_instance - VuurEngine
	#  @param main_player - Player
	#
	# Initializes an Interface object, which is used by the player to play the game
	def __init__(self, engine_instance, main_player):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		self.main_player = main_player
		
	# name: Interpreter.gameConsole
	# @return 0 - Int
	#
	# The console players issue commands through
	def gameConsole(self):
		#
		self.main_player.playerChangeRoom(None, look_again=True)
		self.engine_instance.refreshAll()
		#
		while (break_interface_loop == False):
			try:
				command = input('> ').lower()
				print()
				#
				# Game console built in commands
				if ((command == "exit") or (command == "quit") or (command == "close") or (command == "clear") or (command == ":q") or (command == ":q!")):
					self.engine_instance.exitGame()
				elif (command == "debug"):
					self.engine_instance.debugConsole()
				elif (command == "help"):
					self.engine_instance.gameHelp()
				elif (command == "info"):
					self.engine_instance.gameInfo()
				elif ((command == "stats") or (command == "health") or (command == "turns")):
					self.main_player.playerStats()
				# Or action commands
				else:
					self.parseCommand(command)
			# Handles "Ctrl+C"
			except KeyboardInterrupt:
				print()
				print()
				self.engine_instance.exitGame()
			#
			# Handles "Ctrl+D"
			except EOFError:
				print()
				print()
				self.engine_instance.exitGame()
		return 0
	
	# # # #
	# Private Functions
	# # # #
	
	#  name: Interpreter.attack
	#  @param target_mob - Mob or Questmaster
	#  @return 0 - Int
	#
	# The player attacks another mob
	def attack(self, target_mob):
		#if ((target_mob.casefold() == "self".casefold()) or (target_mob.casefold() == "myself".casefold()) or (target_mob.casefold() == self.main_player.getInfo('name').casefold())):
		if ((target_mob.casefold() in [this_synonym.casefold() for this_synonym in self.main_player.getInfo('synonyms')]) or (target_mob.casefold() == self.main_player.getInfo('name').casefold())):
			self.main_player.setInfo('health', 0)
			print("You could have just typed 'quit' instead of doing that...")
		else:
			actual_target = None
			#
			current_location = self.main_player.getInfo('location')
			location_who = current_location.getInfo('who_is_here')
			for this_mob in location_who:
				if ((this_mob.getInfo('name').casefold() == target_mob.casefold()) and (this_mob != self)):
					actual_target = this_mob
					break
			if (actual_target == None):
				for this_mob in location_who:
					if (target_mob.casefold() in [this_synonym.casefold() for this_synonym in this_mob.getInfo('synonyms')] ):
						target_yorn = input("Did you mean " + this_mob.getInfo('name') + "? [Y]es or [N]o? ").lower()
						print()
						if ((target_yorn == "y") or (target_yorn == "yes")):
							actual_target = this_mob
							break
						else:
							pass
			#
			if (actual_target != None):
				this_fight = Combat(self.engine_instance, self.main_player, actual_target)
				this_fight.playerAttack()
			else:
				print("You do not see them here...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.drop
	#  @param target_item - Item
	#  @return 0 - Int
	#
	# The player drops an item
	def drop(self, target_item):
		if ((target_item.casefold() == "all".casefold()) or (target_item.casefold() == "everything".casefold())):
			if (len(self.main_player.getInfo('inventory')) > 0):
				for this_item in self.main_player.getInfo('inventory'):
					self.main_player.getInfo('location').giveItem(this_item)
				self.main_player.getInfo('inventory').clear()
				print("You drop all items from your inventory...")
			else:
				print("You have no items in your inventory to drop...")
		else:
			actual_target = None
			#
			for this_item in self.main_player.getInfo('inventory'):
				if (this_item.getInfo('name').casefold() == target_item.casefold()):
					actual_target = this_item
					break
			if (actual_target == None):
				for this_item in self.main_player.getInfo('inventory'):
					if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
						#
						this_item_printed_name = this_item.getInfo('name')
						if ((this_item.getInfo('name').casefold().startswith("the ") == False) and (this_item.getInfo('name').casefold().startswith("a ") == False) and (this_item.getInfo('name').casefold().startswith("an ") == False)):
							this_item_printed_name = "the " + this_item.getInfo('name')
						#
						target_yorn = input("Did you mean " + this_item_printed_name + "? [Y]es or [N]o? ").lower()
						print()
						if ((target_yorn == "y") or (target_yorn == "yes")):
							actual_target = this_item
							break
						else:
							pass
			#
			if (actual_target != None):
				self.main_player.removeItem(actual_target)
				self.main_player.getInfo('location').giveItem(actual_target)
				#
				actual_target_printed_name = actual_target.getInfo('name')
				if ((actual_target.getInfo('name').casefold().startswith("the ") == False) and (actual_target.getInfo('name').casefold().startswith("a ") == False) and (actual_target.getInfo('name').casefold().startswith("an ") == False)):
					actual_target_printed_name = "the " + actual_target.getInfo('name')
				#
				print("You drop " + actual_target_printed_name + "...")
			else:
				print("You do not have that item...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.equip
	#  @param target_item - Weapon
	#  @return 0 - Int
	#
	# The player equips a weapon from their inventory
	def equip(self, target_item):
		actual_target = None
		#
		for this_item in self.main_player.getInfo('inventory'):
			if (this_item.getInfo('name').casefold() == target_item.casefold()):
				actual_target = this_item
				break
		if (actual_target == None):
			for this_item in self.main_player.getInfo('inventory'):
				if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
					#
					this_item_printed_name = this_item.getInfo('name')
					if ((this_item.getInfo('name').casefold().startswith("the ") == False) and (this_item.getInfo('name').casefold().startswith("a ") == False) and (this_item.getInfo('name').casefold().startswith("an ") == False)):
						this_item_printed_name = "the " + this_item.getInfo('name')
					#
					target_yorn = input("Did you mean " + this_item_printed_name + "? [Y]es or [N]o? ").lower()
					print()
					if ((target_yorn == "y") or (target_yorn == "yes")):
						actual_target = this_item
						break
					else:
						pass
		#
		if (actual_target != None):
			if (type(actual_target) == Weapon):
				#
				actual_target_printed_name = actual_target.getInfo('name')
				if ((actual_target.getInfo('name').casefold().startswith("the ") == False) and (actual_target.getInfo('name').casefold().startswith("a ") == False) and (actual_target.getInfo('name').casefold().startswith("an ") == False)):
					actual_target_printed_name = "the " + actual_target.getInfo('name')
				#
				equipped_success = self.main_player.equip(actual_target)
				if (self.main_player.getInfo('equipped') == actual_target):
					print("You equip " + actual_target_printed_name + "...")
				else:
					print("Something went wrong equipping " + actual_target_printed_name + "...")
			else:
				print("You cannot equip that item...")
		else:
			print("You do not have that item...")
		#
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.equipped
	#  @return 0 - Int
	#
	# Prints what the player currently has equipped
	def equipped(self):
		equipped_item = self.main_player.getInfo('equipped')
		if (equipped_item != None):
			print(equipped_item.getInfo('name'))
		else:
			print("You have nothing equipped...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.examine
	#  @param target_item - Item, Weapon, or Consumable
	#  @return 0 - Int
	#
	# The player examines an item
	def examine(self, target_item):
		actual_target = None
		total_inventory = []
		#
		current_location = self.main_player.getInfo('location')
		for this_item in current_location.getInfo('inventory'):
			total_inventory.append(this_item)
		for this_item in self.main_player.getInfo('inventory'):
			total_inventory.append(this_item)
		if (self.main_player.getInfo('equipped') != None):
			total_inventory.append(self.main_player.getInfo('equipped'))
		#
		for this_item in total_inventory:
			if (this_item.getInfo('name').casefold() == target_item.casefold()):
				actual_target = this_item
				break
		if (actual_target == None):
			for this_item in total_inventory:
				if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
					#
					actual_target_printed_name = actual_target.getInfo('name')
					if ((actual_target.getInfo('name').casefold().startswith("the ") == False) and (actual_target.getInfo('name').casefold().startswith("a ") == False) and (actual_target.getInfo('name').casefold().startswith("an ") == False)):
						actual_target_printed_name = "the " + actual_target.getInfo('name')
					#
					target_yorn = input("Did you mean " + actual_target_printed_name + "? [Y]es or [N]o? ").lower()
					print()
					if ((target_yorn == "y") or (target_yorn == "yes")):
						actual_target = this_item
						break
					else:
						pass
		#
		if (actual_target != None):
			print(actual_target.getInfo('description'))
		else:
			print("You do not see that item...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.give
	#  @param target_item - Item, Weapon, or Consumable
	#  @param target_mob - Mob or Questmaster
	#  @return 0 - Int
	#
	# The player give an item to a mob
	def give(self, target_item, target_mob):
		actual_target_item = None
		actual_target_mob = None
		#
		for this_item in self.main_player.getInfo('inventory'):
			if (this_item.getInfo('name').casefold() == target_item.casefold()):
				actual_target_item = this_item
				break
		if (actual_target_item == None):
			for this_item in self.main_player.getInfo('inventory'):
				if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
					#
					this_item_printed_name = this_item.getInfo('name')
					if ((this_item.getInfo('name').casefold().startswith("the ") == False) and (this_item.getInfo('name').casefold().startswith("a ") == False) and (this_item.getInfo('name').casefold().startswith("an ") == False)):
						this_item_printed_name = "the " + this_item.getInfo('name')
					#
					item_target_yorn = input("Did you mean " + this_item_printed_name + "? [Y]es or [N]o? ").lower()
					print()
					if ((item_target_yorn == "y") or (item_target_yorn == "yes")):
						actual_target_item = this_item
						break
					else:
						pass
		#	
		current_location = self.main_player.getInfo('location')
		location_who = current_location.getInfo('who_is_here')
		for this_mob in location_who:
			if ((this_mob.getInfo('name').casefold() == target_mob.casefold()) and (this_mob != self.main_player)):
				actual_target_mob = this_mob
				break
		if (actual_target_mob == None):
			for this_mob in location_who:
				if (target_mob.casefold() in [this_synonym.casefold() for this_synonym in this_mob.getInfo('synonyms')]):
					mob_target_yorn = input("Did you mean " + this_mob.getInfo('name') + "? [Y]es or [N]o? ").lower()
					print()
					if ((mob_target_yorn == "y") or (mob_target_yorn == "yes")):
						actual_target_mob = this_mob
						break
					else:
						pass
		#
		if ((actual_target_item != None) and (actual_target_mob != None)):
			self.main_player.removeItem(actual_target_item)
			actual_target_mob.giveItem(actual_target_item)
			#
			actual_target_item_printed_name = actual_target_item.getInfo('name')
			if ((actual_target_item.getInfo('name').casefold().startswith("the ") == False) and (actual_target_item.getInfo('name').casefold().startswith("a ") == False) and (actual_target_item.getInfo('name').casefold().startswith("an ") == False)):
				actual_target_item_printed_name = "the " + actual_target_item.getInfo('name')
			#
			print("You give " + actual_target_item_printed_name + " to " + actual_target_mob.getInfo('name') + "...")
		else:
			if (actual_target_mob == None):
				print("You do not see them here...")
			if (actual_target_item == None):
				print("You do not have that item...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.inventory
	#  @return 0 - Int
	#
	# Prints what items are currently in the player's inventory
	def inventory(self):
		named_inventory = []
		for inventory_item in self.main_player.getInfo('inventory'):
			named_inventory.append(inventory_item.getInfo('name'))
		if (len(named_inventory) > 0):
			print(named_inventory)
		else:
			print("You have no items in your inventory...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.look
	#  @return 0 - Int
	#
	# The player looks around the room they are currently located in
	def look(self):
		self.main_player.playerChangeRoom(None, look_again=True)
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.move
	#  @param target_direction - String
	#  @return 0 - Int
	#
	# The player moves to the room to the target direction of their current location
	def move(self, target_direction):
		current_location = self.main_player.getInfo('location')
		location_neighbors = current_location.getInfo('neighbors')
		#
		if (target_direction == "weast"):
			print("wumbo")
		else:
			if (target_direction in location_neighbors.keys()):
				if (location_neighbors[target_direction] != None):
					self.main_player.playerChangeRoom(target_direction)
				else:
					print("There is nothing that direction...")
			else:
				# If a name is supplied rather than a direction...
				direction_to_move = None
				# Try to find it
				for this_location in location_neighbors:
					if ((location_neighbors[this_location] != None) and (target_direction.casefold() == location_neighbors[this_location].getInfo('name').casefold())):
						direction_to_move = this_location
				if (direction_to_move != None):
					self.main_player.playerChangeRoom(direction_to_move)
				else:
					print("There is no room named that here...")
		#
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.look
	#  @return 0 - Int
	#
	# Prints the player's questlog
	def questlog(self):
		named_questlog = []
		for questlog_entry in self.main_player.getInfo('questlog'):
			named_questlog.append(questlog_entry.getInfo('name'))
		if (len(named_questlog) > 0):
			print(named_questlog)
		else:
			print("You have no active quests...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.read
	#  @param target_item - Item, Weapon, or Consumable
	#  @return 0 - Int
	#
	# The player reads the text on an Item
	def read(self, target_item):
		actual_target = None
		total_inventory = []
		#
		current_location = self.main_player.getInfo('location')
		for this_item in current_location.getInfo('inventory'):
			total_inventory.append(this_item)
		for this_item in self.main_player.getInfo('inventory'):
			total_inventory.append(this_item)
		if (self.main_player.getInfo('equipped') != None):
			total_inventory.append(self.main_player.getInfo('equipped'))
		#
		for this_item in total_inventory:
			if (this_item.getInfo('name').casefold() == target_item.casefold()):
				actual_target = this_item
				break
		if (actual_target == None):
			for this_item in total_inventory:
				if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
					#
					this_item_printed_name = this_item.getInfo('name')
					if ((this_item.getInfo('name').casefold().startswith("the ") == False) and (this_item.getInfo('name').casefold().startswith("a ") == False) and (this_item.getInfo('name').casefold().startswith("an ") == False)):
						this_item_printed_name = "the " + this_item.getInfo('name')
					#
					target_yorn = input("Did you mean " + this_item_printed_name + "? [Y]es or [N]o? ").lower()
					print()
					if ((target_yorn == "y") or (target_yorn == "yes")):
						actual_target = this_item
						break
					else:
						pass
		#
		if (actual_target != None):
			actual_target.readItem()
		else:
			print("You do not see that item...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.take
	#  @param target_item - Item, Weapon, or Consumable
	#  @return 0 - Int
	#
	# The player takes an item and adds it to their inventory
	def take(self, target_item):
		actual_target = None
		#
		current_location = self.main_player.getInfo('location')
		location_inventory = current_location.getInfo('inventory')
		# Take all items in a room
		if ((target_item.casefold() == "all".casefold()) or (target_item.casefold() == "everything".casefold())):
			if (len(location_inventory) > 0):
				for this_item in location_inventory:
					self.main_player.giveItem(this_item)
				location_inventory.clear()
				print("You take all the items the room...")
			else:
				print("There are no items here to take...")
		# Take specific item from room
		else:
			for this_item in location_inventory:
				if (this_item.getInfo('name').casefold() == target_item.casefold()):
					actual_target = this_item
					break
			if (actual_target == None):
				for this_item in location_inventory:
					if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
						#
						this_item_printed_name = this_item.getInfo('name')
						if ((this_item.getInfo('name').casefold().startswith("the ") == False) and (this_item.getInfo('name').casefold().startswith("a ") == False) and (this_item.getInfo('name').casefold().startswith("an ") == False)):
							this_item_printed_name = "the " + this_item.getInfo('name')
						#
						target_yorn = input("Did you mean " + this_item_printed_name + "? [Y]es or [N]o? ").lower()
						print()
						if ((target_yorn == "y") or (target_yorn == "yes")):
							actual_target = this_item
							break
						else:
							pass
			#
			if (actual_target != None):
				self.main_player.getInfo('location').removeItem(actual_target)
				self.main_player.giveItem(actual_target)
				#
				actual_target_printed_name = actual_target.getInfo('name')
				if ((actual_target.getInfo('name').casefold().startswith("the ") == False) and (actual_target.getInfo('name').casefold().startswith("a ") == False) and (actual_target.getInfo('name').casefold().startswith("an ") == False)):
					actual_target_printed_name = "the " + actual_target.getInfo('name')
				#
				print("You took " + actual_target_printed_name + "...")
			else:
				print("You don't see that item here...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.talk
	#  @param target_mob - Mob or Questmaster
	#  @return 0 - Int
	#
	# The player talks to a mob
	def talk(self, target_mob):
		actual_target = None
		#
		if ((target_mob.casefold() in [this_synonym.casefold() for this_synonym in self.main_player.getInfo('synonyms')]) or (target_mob.casefold() == self.main_player.getInfo('name').casefold())):
			brain_juice = ''.join(random.choice(self.main_player.getInfo('name')) for i in range(25))
			print(self.engine_instance.vuurPassword(brain_juice))
		else:
			current_location = self.main_player.getInfo('location')
			location_who = current_location.getInfo('who_is_here')
			#
			for this_mob in location_who:
				if ((this_mob.getInfo('name').casefold() == target_mob.casefold()) and (this_mob != self.main_player)):
					actual_target = this_mob
					break
			if (actual_target == None):
				for this_mob in location_who:
					if (target_mob.casefold() in [this_synonym.casefold() for this_synonym in this_mob.getInfo('synonyms')]):
						target_yorn = input("Did you mean " + this_mob.getInfo('name') + "? [Y]es or [N]o? ").lower()
						print()
						if ((target_yorn == "y") or (target_yorn == "yes")):
							actual_target = this_mob
							break
						else:
							pass
			#
			if (actual_target != None):
				if (type(actual_target) == Questmaster):
					actual_target.questmasterTalk(self.main_player)
				elif (type(actual_target == Player) or type(actual_target == Mob)):
					if (len(actual_target.getInfo('talking_points')) > 0):
						actual_target.talk()
					else:
						print("It appears they don't have much to say...")
			else:
				print("You do not see them here...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.unequip
	#  @return 0 - Int
	#
	# The player unequips their current equipped weapon
	def unequip(self):
		item_to_unequip = self.main_player.getInfo('equipped')
		if (item_to_unequip != None):
			self.main_player.unequip()
			#
			item_to_unequip_printed_name = item_to_unequip.getInfo('name')
			if ((item_to_unequip.getInfo('name').casefold().startswith("the ") == False) and (item_to_unequip.getInfo('name').casefold().startswith("a ") == False) and (item_to_unequip.getInfo('name').casefold().startswith("an ") == False)):
				item_to_unequip_printed_name = "the " + item_to_unequip.getInfo('name')
			#
			if (self.main_player.getInfo('equipped') == None):
				print("You unequip " + item_to_unequip_printed_name + "...")
			else:
				print("Something went wrong tring to unequip " + item_to_unequip_printed_name + "...")
		else:
			print("You don't have anything equipped...")
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.use
	#  @param target_item - Consumable
	#  @return 0 - Int
	#
	# The player uses a consumable item
	def use(self, target_item):
		actual_target = None
		#
		for this_item in self.main_player.getInfo('inventory'):
			if (this_item.getInfo('name').casefold() == target_item.casefold()):
				actual_target = this_item
				break
		if (actual_target == None):
			for this_item in self.main_player.getInfo('inventory'):
				if (target_item.casefold() in [this_synonym.casefold() for this_synonym in this_item.getInfo('synonyms')]):
					#
					this_item_printed_name = this_item.getInfo('name')
					if ((this_item.getInfo('name').casefold().startswith("the ") == False) and (this_item.getInfo('name').casefold().startswith("a ") == False) and (this_item.getInfo('name').casefold().startswith("an ") == False)):
						this_item_printed_name = "the " + this_item.getInfo('name')
					#
					target_yorn = input("Did you mean " + this_item_printed_name + "? [Y]es or [N]o? ").lower()
					print()
					if ((target_yorn == "y") or (target_yorn == "yes")):
						actual_target = this_item
						break
					else:
						pass
		#
		if (actual_target != None):
			if (type(actual_target) == Consumable):
				#
				actual_target_printed_name = actual_target.getInfo('name')
				if ((actual_target.getInfo('name').casefold().startswith("the ") == False) and (actual_target.getInfo('name').casefold().startswith("a ") == False) and (actual_target.getInfo('name').casefold().startswith("an ") == False)):
					actual_target_printed_name = "the " + actual_target.getInfo('name')
				#
				self.main_player.consumeItem(actual_target)
				print("You use " + actual_target_printed_name + "...")
			else:
				print("You cannot use that item...")
		else:
			print("You do not have that item...")
		#
		self.engine_instance.refreshAll()
		return 0

	#  name: Interpreter.invalid
	#  @return 0 - Int
	#
	# Handles invalid commands issued by the player
	def invalid(self):
		print("You're not sure what to do...")
		self.engine_instance.refreshAll()
		return 0

	# name: Interpreter.parseCommand
	# @param command - String
	# @return 0 - Int
	#
	# Parses commands issued by the player
	def parseCommand(self, command):
		# attack command
		if (command.startswith("attack")  or command.startswith("fight") or  command.startswith("kill")):
			if (command.startswith("attack ") or command.startswith("fight ") or  command.startswith("kill ")):
				target = re.split("attack |fight |kill ", command)
				if (len(target) == 2):
					self.attack(target[1].lstrip().rstrip())
				else:
					print("You need to specify who to attack...")
			else:
				print("You need to specify who to attack...")
		# drop command
		elif (command.startswith("drop") or command.startswith("toss")):
			if (command.startswith("drop ") or command.startswith("toss ")):
				target = re.split("drop the |toss the |drop |toss ", command)
				if (len(target) == 2):
					self.drop(target[1].lstrip().rstrip())
				else:
					print("You need to specify what to drop...")
			else:
				print("You need to specify what to drop...")
		# equip command
		elif ((command.startswith("equip") and (command != "equipped"))or command.startswith("wield")):
			if (command.startswith("equip ") or command.startswith("wield ")):
				target = re.split("equip the |wield the |equip |wield ", command)
				if (len(target) == 2):
					self.equip(target[1].lstrip().rstrip())
				else:
					print("You need to specify what to equip...")
			else:
				print("You need to specify what to equip...")
		# equipped command
		elif (command == "equipped"):
			self.equipped()
		# examine command
		elif (command.startswith("examine") or command.startswith("check")):
			if (command.startswith("examine ") or command.startswith("check ")):
				target = re.split("examine the |check the |examine |check ", command)
				if (len(target) == 2):
					self.examine(target[1].lstrip().rstrip())
				else:
					print("You need to specify what to examine...")
			else:
				print("You need to specify what to examine...")
		# give command
		elif (command.startswith("give") or command.startswith("hand")):
			if (command.startswith("give ") or command.startswith("hand ")):
				target = re.split("give |hand |to ", command)
				if (len(target) == 3):
					self.give(target[1].lstrip().rstrip(), target[2].lstrip().rstrip())
				else:
					print("You need to specify what to give and who to give it to...")
			else:
				print("You need to specift what to give and who to give it to...")
		# inventory command
		elif (command == "inventory"):
			self.inventory()
		# look command
		elif ((command == "look")):
			self.look()
		# move command
		elif (command.startswith("move ") or command.startswith("go ") or (command == "north") or (command == "south") or (command == "east") or (command == "west") or (command == "up") or (command == "down") or (command == "weast")):
			if (command.startswith("move ") or command.startswith("go ")):
				target = re.split("move |go ", command)
				if (len(target) == 2):
					self.move(target[1].lstrip().rstrip())
				else:
					print("You need to specify where to go...")
			else:
				self.move(command)
		# questlog command
		elif ((command == "questlog") or (command == "quests")):
			self.questlog()
		# read command
		elif (command.startswith("read")):
			if (command.startswith("read ")):
				target = re.split("read the |read ", command)
				if (len(target) == 2):
					self.read(target[1].lstrip().rstrip())
				else:
					print("You need to specify what to read...")
			else:
				print("You need to specify what to read...")
		# take command
		elif (command.startswith("take") or command.startswith("grab") or command.startswith("pick up") or command.startswith("get")):
			if (command.startswith("take ") or command.startswith("grab ") or command.startswith("pick up ") or command.startswith("get ")):
				target = re.split("take the |grab the |pick up the |get the |take |grab |pick up |get ", command)
				if (len(target) == 2):
					self.take(target[1].lstrip().rstrip())
				else:
					print("You need to specify what to take...")
			else:
				print("You need to specify what to take...")
		# talk command
		elif (command.startswith("talk with") or command.startswith("speak with") or command.startswith("talk to") or command.startswith("speak to") or command.startswith("talk") or command.startswith("speak")):
			if (command.startswith("talk with ") or command.startswith("speak with ") or command.startswith("talk to ") or command.startswith("speak to ") or command.startswith("talk ") or command.startswith("speak ")):
				target = re.split("talk with |speak with |talk to |speak to |talk |speak ", command)
				if (len(target) == 2):
					self.talk(target[1].lstrip().rstrip())
				else:
					print("You need to specify who to talk to...")
			else:
				print("You need to specify who to talk to...")
		# unequip command
		elif ((command =="unequip") or (command =="unwield")):
			self.unequip()
		# use command
		elif (command.startswith("use") or command.startswith("consume")):
			if (command.startswith("use ") or command.startswith("consume ")):
				target = re.split("use the |consume the |use |consume ", command)
				if (len(target) == 2):
					self.use(target[1].lstrip().rstrip())
				else:
					print("You need to specify what to use...")
			else:
				print("You need to specify what to use...")
		# invalid command
		else:
			self.invalid()
		return 0

#  name: Item
#
#  An Item is a physical item that can be interacted with in some way
#  as part of a game.
class Item:
	
	# # # #
	# Public Functions
	# # # #
	
	#  name: Item.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param description - String
	#  @param synonyms - List or None (optional)
	#
	# Initializes an Item object
	def __init__(self, engine_instance, name, description, **kwargs):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Item info
		self.item_info = {}
		self.item_info['name'] = name
		self.item_info['description'] = description
		self.item_info['can_equip'] = False
		self.item_info['can_use'] = False
		self.item_info['writing'] = None
		self.item_info['synonyms'] =  kwargs.get('synonyms', [])
		#
		# Every Item and Item-derived object has a synonym of "the" + its name
		if (self.item_info['name'].casefold().startswith('the ') == False):
			self.item_info['synonyms'].append('the ' + self.item_info['name'].casefold())

	#  name: Item.getInfo
	#  @param info - String
	#  @return to_return - *
	#
	# Returns requested info from 'item_info' dictionary
	def getInfo(self, info):
		to_return = None
		if (info in self.item_info):
			to_return = self.item_info[info]
		return to_return

	#  name: Item.setInfo
	#  @param info - String
	#  @param value - *
	#  @return 0 - Int
	#
	# Sets the value for key (determined by 'info' param) in the 'item_info' dictionary
	def setInfo(self, info, value):
		if (info in self.item_info):
			self.item_info[info] = value
		return 0

	#  name: Item.setWriting
	#  @param info - String
	#  @param value - *
	#  @return 0 - Int
	#
	# Specifically sets the value of the 'writing' key in the 'item_info' dictionary
	def setWriting(self, writing):
		self.item_info['writing'] = writing
		return 0

	#  name: Item.readItem
	#  @return 0 - Int
	#
	# Prints the value of the 'writing' key in the 'item_info' dictionary
	def readItem(self):
		if (self.item_info['writing'] == None):
			print("Nothing is writen on this item...")
		else:
			print(self.item_info['writing'])
		return 0

#  name: Consumable
#
#  A Consumable is an Item that can be consumed by a Mob and has some
#  kind of impact on the Mob's health or attack.
class Consumable(Item):
	
	# # # #
	# Public Functions
	# # # #

	#  name: Consumable.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param description - String
	#  @param attack_buff - Int
	#  @param health_buff - Int
	#  @param synonyms - List or None (optional)
	#
	# Initializes a Consumable object, which extends the Item class
	def __init__(self, engine_instance, name, description, attack_buff, health_buff, **kwargs):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Item info
		self.item_info = {}
		self.item_info['name'] = name
		self.item_info['description'] = description
		self.item_info['can_equip'] = False
		self.item_info['can_use'] = True
		self.item_info['writing'] = None
		self.item_info['synonyms'] =  kwargs.get('synonyms', [])
		# Consumable Info
		self.item_info['attack_buff'] = attack_buff + 2
		self.item_info['health_buff'] = health_buff + 2
		#
		# Every Item and Item-derived object has a synonym of "the" + its name
		if (self.item_info['name'].casefold().startswith('the ') == False):
			self.item_info['synonyms'].append('the ' + self.item_info['name'].casefold())

#  name: Weapon
#
#  A Weapon is an Item that can be equipped by a Mob for use in combat.
class Weapon(Item):
	
	# # # #
	# Public Functions
	# # # #

	#  name: Weapon.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param description - String
	#  @param attack - Int
	#  @param synonyms - List or None (optional)
	#
	# Initializes a Weapon object, which extends the Item class
	def __init__(self, engine_instance, name, description, attack, **kwargs):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Item info
		self.item_info = {}
		self.item_info['name'] = name
		self.item_info['description'] = description
		self.item_info['can_equip'] = True
		self.item_info['can_use'] = False
		self.item_info['writing'] = None
		self.item_info['synonyms'] =  kwargs.get('synonyms', [])
		#
		self.item_info['attack'] = attack
		#
		# Every Item and Item-derived object has a synonym of "the" + its name
		if (self.item_info['name'].casefold().startswith('the ') == False):
			self.item_info['synonyms'].append('the ' + self.item_info['name'].casefold())

#  name: Mob
#
#  A Mob is a living being in a game.
class Mob:
	
	# # # #
	# Public Functions
	# # # #
	
	#  name: Mob.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param location - Room
	#  @param hostile - Bool
	#  @param synonyms - List or None (optional)
	#
	# Initializes a Mob object
	def __init__(self, engine_instance, name, location, hostile, **kwargs):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Mob info
		self.mob_info = {}
		self.mob_info['name'] = name
		self.mob_info['location'] = location
		self.mob_info['hostile'] = hostile
		self.mob_info['health'] = 100
		self.mob_info['dead'] = False
		self.mob_info['attack'] = int(self.mob_info['health'] / 2)
		self.mob_info['talking_points'] = []
		self.mob_info['inventory'] = []
		self.mob_info['equipped']  = None
		self.mob_info['visited_rooms'] = []
		self.mob_info['synonyms'] =  kwargs.get('synonyms', [])
		#
		self.mob_info['location'].visitRoom(self)
		self.mob_info['visited_rooms'].append(self.mob_info['location'])

	#  name: Mob.getInfo
	#  @param info - String
	#  @return to_return - *
	#
	# Returns requested info from 'mob_info' dictionary
	def getInfo(self, info):
		to_return = None
		if (info in self.mob_info):
			to_return = self.mob_info[info]
		return to_return

	#  name: Mob.setInfo
	#  @param info - String
	#  @param value - *
	#  @return 0 - Int
	#
	# Sets the value for key (determined by 'info' param) in the 'mob_info' dictionary
	def setInfo(self, info, value):
		if (info in self.mob_info):
			self.mob_info[info] = value
		return 0

	#  name: Mob.equip
	#  @param item_to_equip - String
	#  @return 0 - Int
	#
	# Specifically sets the value of the 'equipped' key in the 'mob_info' dictionary
	def equip(self, item_to_equip):
		if (self.mob_info['equipped'] != None):
			self.unequip()
		try:
			self.mob_info['inventory'].remove(item_to_equip)
		except:
			pass
		self.mob_info['equipped'] = item_to_equip
		#
		weapon_attack = item_to_equip.getInfo('attack')
		current_attack = self.mob_info['attack']
		updated_attack = current_attack + weapon_attack
		self.mob_info['attack'] = updated_attack
		return 0
	
	#  name: Mob.addTalkingPoint
	#  @param phrase - String
	#  @return 0 - Int
	#
	# Adds 'phrase' to the 'talking_points' list
	def addTalkingPoint(self, phrase):
		self.mob_info['talking_points'].append(phrase)
		return 0

	#  name: Mob.giveItem
	#  @param item_to_give - Item, Weapon, Consumable
	#  @return 0 - Int
	#
	# Adds 'item_to_give' to the mob's inventory
	def giveItem(self, item_to_give):
		#
		self.mob_info['inventory'].append(item_to_give)
		#
		return 0
	
	# # # #
	# Private Functions
	# # # #

	#  name: Mob.removeItem
	#  @param item_to_give - Item, Weapon, Consumable
	#  @return 0 - Int
	#
	# Removes 'item_to_remove' from the mob's inventory
	def removeItem(self, item_to_remove):
		to_return = 0
		if (item_to_remove in self.mob_info['inventory']):
			self.mob_info['inventory'].remove(item_to_remove)
		else:
			to_return = 1
		return to_return

	#  name: Mob.die
	#  @return 0 - Int
	#
	# Sets the value of the 'dead' key in the 'mob_info' dictionary to 'True',
	# empties their inventory into their current location, and removes them from
	# that current location
	def die(self):
		if (self.mob_info['dead'] == True):
			self.unequip()
			for inventory_item in self.mob_info['inventory']:
				if (inventory_item != None):
					self.removeItem(inventory_item)
					self.mob_info['location'].giveItem(inventory_item)
			self.mob_info['location'].leaveRoom(self)
		return 0

	#  name: Mob.unequip
	#  @return 0 - Int
	#
	# Sets the value of the 'equipped' key in the 'mob_info' dictionary to 'None'
	def unequip(self):
		current_attack = self.mob_info['attack']
		if (self.mob_info['equipped'] != None):
			weapon_attack = self.mob_info['equipped'].getInfo('attack')
			updated_attack = current_attack - weapon_attack
			self.mob_info['attack'] = updated_attack
		#
		self.mob_info['inventory'].append(self.mob_info['equipped'])
		self.mob_info['equipped'] = None
		return 0

	#  name: Mob.consumeItem
	#  @param consumed_item - Item, Weapon, Consumable
	#  @return 0 - Int
	#
	# Consume 'consumed_item', adding its buffs, and removing it from the mob's inventory
	def consumeItem(self, consumed_item):
		self.mob_info['inventory'].remove(consumed_item)
		#
		health_buff = consumed_item.getInfo('health_buff')
		self.mob_info['health'] = self.mob_info['health'] + health_buff
		#
		attack_buff = consumed_item.getInfo('attack_buff')
		self.mob_info['attack'] = self.mob_info['attack'] + attack_buff
		#
		return 0

	#  name: Mob.talk
	#  @return 0 - Int
	#
	# Prints a random string from the 'talking_points' list
	def talk(self):
		if (len(self.mob_info['talking_points']) > 0):
			print(self.mob_info['name'] + ': "' + random.choice(self.mob_info['talking_points']) + '"')
		return 0
	
	#  name: Mob.changeRoom
	#  @param new_room - Room
	#  @return to_return - Int
	#
	# Moves mob to a new location
	def changeRoom(self, new_room):
		to_return = 0
		if (new_room != None):
			self.mob_info['location'].leaveRoom(self)
			self.mob_info['location'] = new_room
			self.mob_info['location'].visitRoom(self)
			if (self.mob_info['location'] not in self.mob_info['visited_rooms']):
				self.mob_info['visited_rooms'].append(self.mob_info['location'])
		else:
			to_return = 1
		#
		return to_return

	#  name: Mob.takeDamage
	#  @param amount - Int
	#  @return 0 - Int
	#
	# Reduces mob's health by 'amount'
	def takeDamage(self, amount):
		self.mob_info['health'] = self.mob_info['health'] - int(amount)
		return 0

	#  name: Mob.refresh
	#  @return 0 - Int
	#
	# Checks mob health, and updates stats as needed
	def refresh(self):
		if (self.mob_info['health'] <= 0):
			self.mob_info['dead'] = True
			self.die()
		else:
			#
			health_should_be = 100
			if (self.mob_info['health'] < health_should_be):
				self.mob_info['health'] = self.mob_info['health'] + 1
			elif (self.mob_info['health'] > health_should_be):
				self.mob_info['health'] = self.mob_info['health'] - 2
			#
			attack_should_be = int(self.mob_info['health'] / 2)
			if (self.mob_info['equipped'] != None):
				attack_should_be = attack_should_be + self.mob_info['equipped'].getInfo('attack')
			if (self.mob_info['attack'] < attack_should_be):
				self.mob_info['attack'] = self.mob_info['attack'] + 1
			elif (self.mob_info['attack'] > attack_should_be):
				self.mob_info['attack'] = self.mob_info['attack'] - 2
		return 0
	
#  name: Player
#
#  The Player is the Mob which is directly controlled through
#  the Interpreter as part of playing the game.
class Player(Mob):
	
	# # # #
	# Public functions
	# # # #

	#  name: Player.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param location - Room
	#
	# Initializes a Player object, which extends the Mob class
	def __init__(self, engine_instance, name, location):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Mob info
		self.mob_info = {}
		self.mob_info['name'] = name
		self.mob_info['location'] = location
		self.mob_info['hostile'] = False
		self.mob_info['health'] = 100
		self.mob_info['dead'] = False
		self.mob_info['attack'] = int(self.mob_info['health'] / 2)
		self.mob_info['talking_points'] = []
		self.mob_info['inventory'] = []
		self.mob_info['equipped']  = None
		self.mob_info['visited_rooms'] = []
		self.mob_info['synonyms'] =  ['me', 'myself', 'i', 'self']
		# Player info
		self.mob_info['questlog'] = []
		self.mob_info['turned_in_quests'] = []
		#
		self.mob_info['location'].visitRoom(self)
	
	# # # #
	# Private Functions
	# # # #

	#  name: Player.playerChangeRoom
	#  @param direction - String
	#  @param look_again - Bool or None (optional)
	#  @return 0 - Int
	#
	# Wraps the 'changeRoom()' function and prints relevant information about the player's location
	def playerChangeRoom(self, direction, **kwargs):
		look_again = kwargs.get('look_again', None)
		been_here = False
		unlock_success = None
		#
		if (direction != None):
			this_location_neighbors = self.mob_info['location'].getInfo('neighbors')
			new_location = this_location_neighbors[direction]
			if (new_location in self.mob_info['visited_rooms']):
				been_here = True
			if (new_location != None):
				#
				if (new_location.getInfo('locked') == False):
					new_location.setInfo('hidden', False)
					self.changeRoom(new_location)
				else:
					unlock_success = False
					password = input("Password: ")
					print()
					if (password == new_location.getInfo('password')):
						unlock_success = True
						new_location.setInfo('locked', False)
						new_location.setInfo('hidden', False)
						self.changeRoom(new_location)
					else:
						print("The room remains locked...")
			else:
				print("There is nothing in that direction...")
		elif (direction == None):
			new_location = self.mob_info['location']
			been_here = True
			if (new_location not in self.mob_info['visited_rooms']): 
				self.mob_info['visited_rooms'].append(new_location)
		#
		if ((unlock_success == True) or (unlock_success == None)):
			if ((been_here == False) or (look_again == True)):
				print(self.mob_info['location'].getInfo('name'))
				print(self.mob_info['location'].getInfo('description'))
				#
				who_is_here_list = []
				for who in self.mob_info['location'].getInfo('who_is_here'):
					if ((who != self) and (who != None)):
						who_is_here_list.append(who.getInfo('name'))
				if (len(who_is_here_list) > 0):
					print("You see these beings here: ", who_is_here_list)
				#
				what_is_here_list = []
				for what in self.mob_info['location'].getInfo('inventory'):
					if (what != None):
						what_is_here_list.append(what.getInfo('name'))
				if (len(what_is_here_list) > 0):
					print("You see these items here: ", what_is_here_list)
				#
				neighbor_list = []
				for neighbor_room_direction in self.mob_info['location'].getInfo('neighbors'):
					neighbor_room = self.mob_info['location'].getInfo('neighbors')[neighbor_room_direction]
					if (neighbor_room != None) and (neighbor_room.getInfo('hidden') == False):
						neighbor_string = neighbor_room.getInfo('name') + " (" + neighbor_room_direction.capitalize() + ")"
						neighbor_list.append(neighbor_string)
				if (len(neighbor_list) > 0):
					if (len(neighbor_list) == 1):
						print("There is an adjacent room: ", neighbor_list)
					elif (len(neighbor_list) >= 2):
						print("There are adjacent rooms: ", neighbor_list)
			elif ((been_here == True) and ((look_again == None) or (look_again == False))):
				print(self.mob_info['location'].getInfo('name'))
		#
		hostile_entities = []
		hostile_position = 0
		this_location = self.mob_info['location']
		for possible_hostile in self.mob_info['location'].getInfo('who_is_here'):
			if (possible_hostile.getInfo('hostile') == True):
				hostile_entities.append(possible_hostile)
		if (len(hostile_entities) > 0):
			while ((self.mob_info['location'] == this_location) and (len(hostile_entities) > hostile_position)):
				if (self.mob_info['health'] > 0) and (self.mob_info['dead'] == False):
					print()
					this_fight = Combat(self.engine_instance, self, hostile_entities[hostile_position])
					this_fight.otherAttack()
				hostile_position += 1
		#
		return 0

	#  name: Player.playerRefresh
	#  @return 0 - Int
	#
	# Wraps the 'refresh()' function and exits the game if player has died
	def playerRefresh(self):
		self.refresh()
		final_quest_finished = False
		# Checks status of in-progress quests
		for this_quest in self.mob_info['questlog']:
			this_quest.checkCompletion()
		# Checks for completion of final quest
		for this_quest in self.mob_info['turned_in_quests']:
			this_final = this_quest.getInfo('final_quest')
			if (this_final == True):
				final_quest_finished = True
		# If Player died, print game over
		if (self.mob_info['dead'] == True):
			print()
			print("Game Over: You Died")
			print()
			input("Press Enter to Exit...")
			print()
			self.engine_instance.exitGame()
		# If Player not dead but final quest complete, print game over
		elif (final_quest_finished == True):
			self.engine_instance.gameWon()
			print()
			print("Game Over: You Won!")
			print()
			input("Press Enter to Exit...")
			print()
			self.engine_instance.exitGame()
		#
		return 0

	#  name: Player.playerStats
	#  @return 0 - Int
	#
	# Prints player and game stats
	def playerStats(self):
		self.engine_instance.gameStats()
		print("Health: " + str(self.mob_info['health']))
		print("Attack: " + str(self.mob_info['attack']))
		return 0

	#  name: Player.giveQuest
	#  @param what_quest - Quest
	#  @return 0 - Int
	#
	# Adds a quest to the 'questlog' list
	def giveQuest(self, what_quest):
		if (type(what_quest) == Quest):
			self.mob_info['questlog'].append(what_quest)
		return 0

	#  name: Player.finishQuest
	#  @param what_quest - Quest
	#  @return 0 - Int
	#
	# Removes a quest from the 'questlog' list and adds it to the 'turned_in_quests' list
	def finishQuest(self, what_quest):
		if (type(what_quest) == Quest):
			self.mob_info['questlog'].remove(what_quest)
			self.mob_info['turned_in_quests'].append(what_quest)
		return 0

#  name: Questmaster
#
#  The Questmaster is the Mob which is provides Quests to the Player.
class Questmaster(Mob):
	
	# # # #
	# Public Functions
	# # # #

	#  name: Questmaster.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param location - Room
	#  @param synonyms - List or None (optional)
	#
	# Initializes a Questmaster object, which extends the Mob class
	def __init__(self, engine_instance, name, location, **kwargs):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Mob info
		self.mob_info = {}
		self.mob_info['name'] = name
		self.mob_info['location'] = location
		self.mob_info['hostile'] = False
		self.mob_info['health'] = 100
		self.mob_info['dead'] = False
		self.mob_info['attack'] = int(self.mob_info['health'] / 2)
		self.mob_info['talking_points'] = []
		self.mob_info['inventory'] = []
		self.mob_info['equipped']  = None
		self.mob_info['visited_rooms'] = []
		self.mob_info['synonyms'] = kwargs.get('synonyms', [])
		# Questmaster info
		self.mob_info['quests_to_give'] = []
		#
		self.mob_info['location'].visitRoom(self)
		self

	#  name: Questmaster.addQuestToGive
	#  @param quest_to_add - Quest
	#  @return 0 - Int
	#
	# Adds a quest for the questmaster to give
	def addQuestToGive(self, quest_to_add):
		if (type(quest_to_add) == Quest):
			self.mob_info['quests_to_give'].append(quest_to_add)
			quest_to_add.setInfo('questgiver', self)
		return 0	

	# # # #
	# Private Functions
	# # # #

	#  name: Questmaster.questmasterTalk
	#  @return 0 - Int
	#
	# Wraps the 'talk()' function and provides interface for the questmaster to give quests
	def questmasterTalk(self, this_player):
		self.talk()
		#
		quest_to_turn_in = None
		for this_quest in self.mob_info['quests_to_give']:
			is_in_progress = this_quest in this_player.getInfo('questlog')
			has_been_completed = this_quest.getInfo('completed')
			not_already_turned_in = this_quest not in this_player.getInfo('turned_in_quests')
			#
			if ((is_in_progress == True) and (has_been_completed == True) and (not_already_turned_in == True)):
				quest_to_turn_in = this_quest
				break
		#
		if (quest_to_turn_in != None):
			print(self.mob_info['name'] + ': "' + quest_to_turn_in.getInfo('quest_completion_text') + '"')
			quest_to_turn_in.endQuest()
			
		elif (quest_to_turn_in == None):
			#
			eligible_quests = []
			selected_quest = None
			#
			not_already_in_progress = False
			not_already_turned_in = False
			#
			for this_quest in self.mob_info['quests_to_give']:
				if ((this_quest in this_player.getInfo('turned_in_quests')) or (this_quest in this_player.getInfo('questlog'))):
					pass
				else:
					prereqs_completed = True
					for quest_prereq in this_quest.getInfo('prereqs'):
						if quest_prereq not in this_player.getInfo('turned_in_quests'):
							prereqs_completed = False
							break
					if (prereqs_completed == True):
						eligible_quests.append(this_quest)
			#
			if (len(eligible_quests) > 0):
				print(self.mob_info['name'] + ': "I can offer you the following quests:"')
				quest_position = 0
				quest_number_list = []
				quest_number_list.append(0)
				print("0 - None")
				while (len(eligible_quests) > quest_position):
					quest_name = eligible_quests[quest_position].getInfo('name')
					quest_number = quest_position + 1
					print(str(quest_number) + " - " + quest_name)
					quest_number_list.append(quest_number)
					quest_position += 1
				what_quest = input("What quest would you like? ")
				print()
				if (re.search("\d", what_quest)):
					what_quest = int(what_quest)
					if (what_quest == 0):
						pass
					elif ((what_quest != 0) and (len(eligible_quests) >= what_quest)):
						what_quest -= 1
						selected_quest = eligible_quests[what_quest]
					else:
						print(self.mob_info['name'] + ': "That is not a quest I offer."')
						
				else:
					quest_found = False
					if (what_quest.casefold() == "none"):
						pass
					else:
						for eligible_quest_entry in eligible_quests:
							if (what_quest.casefold() == eligible_quest_entry.getInfo('name').casefold()):
								selected_quest = eligible_quest_entry
								quest_found = True
								break
						if (quest_found == False):
							print(self.mob_info['name'] + ': "That is not a quest I offer."')
			#
			if (selected_quest != None):
				print(self.mob_info['name'] + ': "' + selected_quest.getInfo('quest_description') + '"')
				selected_quest.startQuest(this_player)
		#
		return 0

#  name: Player
#
#  A Room is a location within the game.
class Room:
	
	# # # #
	# Public Functions
	# # # #
	
	#  name: Room.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param locked - Bool
	#  @param hidden - Bool
	#  @param password - String (optional)
	#
	# Initializes a Room object
	def __init__(self, engine_instance, name, locked, hidden, **kwargs):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		# Room info
		self.room_info = {}
		self.room_info['name'] = name
		self.room_info['description'] = "Yet another poorly lit dungeon room."
		self.room_info['locked'] = locked
		self.room_info['password'] =  kwargs.get('password', None)
		self.room_info['hidden'] = hidden
		self.room_info['inventory'] = []
		self.room_info['who_is_here'] = []
		# Initialize neighboring Rooms
		self.room_info['neighbors'] = {}
		self.room_info['neighbors']['north'] = None
		self.room_info['neighbors']['south'] = None
		self.room_info['neighbors']['east'] = None
		self.room_info['neighbors']['west'] = None
		self.room_info['neighbors']['up'] = None
		self.room_info['neighbors']['down'] = None
		# Room password
		if (self.room_info['locked'] == True) and (self.room_info['password'] == None):
			self.room_info['password'] = self.engine_instance.vuurPassword(self.room_info['name'])

	#  name: Room.getInfo
	#  @param info - String
	#  @return to_return - *
	#
	# Returns requested info from 'room_info' dictionary
	def getInfo(self, info):
		to_return = None
		if (info in self.room_info):
			to_return = self.room_info[info]
		return to_return

	#  name: Room.setInfo
	#  @param info - String
	#  @param value - *
	#  @return 0 - Int
	#
	# Sets the value for key (determined by 'info' param) in the 'room_info' dictionary
	def setInfo(self, info, value):
		if (info in self.room_info):
			self.room_info[info] = value
		return 0

	#  name: Room.giveItem
	#  @param item_to_give - Item, Weapon, Consumable
	#  @return 0 - Int
	#
	# Adds 'item_to_give' to the room's inventory
	def giveItem(self, item_to_give):
		#
		self.room_info['inventory'].append(item_to_give)
		#
		return 0

	#  name: Room.linkRoom
	#  @param north - Room or None
	#  @param south - Room or None
	#  @param east - Room or None
	#  @param west - Room or None
	#  @param up - Room or None
	#  @param down - Room or None
	#  @return 0 - Int
	#
	# Connects rooms to one another
	def linkRoom(self, north, south, east, west, up, down):
		#
		if (north != None):
			self.room_info['neighbors']['north'] = north
		else:
			self.room_info['neighbors']['north'] = None
		#
		if (south != None):
			self.room_info['neighbors']['south'] = south
		else:
			self.room_info['neighbors']['south'] = None
		#
		if (east != None):
			self.room_info['neighbors']['east'] = east
		else:
			self.room_info['neighbors']['east'] = None
		#
		if (west != None):
			self.room_info['neighbors']['west'] = west
		else:
			self.room_info['neighbors']['west'] = None
		#
		if (up != None):
			self.room_info['neighbors']['up'] = up
		else:
			self.room_info['neighbors']['up'] = None
		#
		if (down != None):
			self.room_info['neighbors']['down'] = down
		else:
			self.room_info['neighbors']['down'] = None
		#
		return 0
	
	# # # #
	# Private Functions
	# # # #

	#  name: Room.removeItem
	#  @param item_to_give - Item, Weapon, Consumable
	#  @return 0 - Int
	#
	# Removes 'item_to_remove' from the room's inventory
	def removeItem(self, item_to_remove):
		to_return = 0
		if (item_to_remove in self.room_info['inventory']):
			self.room_info['inventory'].remove(item_to_remove)
		else:
			to_return = 1
		return to_return

	#  name: Room.visitRoom
	#  @param visiting_mob - Mob, Player, Questmaster
	#  @return 0 - Int
	#
	# Adds 'visiting_mob' to the 'who_is_here' list
	def visitRoom(self, visiting_mob):
		self.room_info['who_is_here'].append(visiting_mob)
		return 0

	#  name: Room.leaveRoom
	#  @param visiting_mob - Mob, Player, Questmaster
	#  @return 0 - Int
	#
	# Removes 'visiting_mob' from the 'who_is_here' list
	def leaveRoom(self, leaving_mob):
		if (leaving_mob in self.room_info['who_is_here']):
			self.room_info['who_is_here'].remove(leaving_mob)
		return 0

	#  name: Room.refresh
	#  @return 0 - Int
	#
	# Refreshes the state of the room, including checking for hostile mobs
	# and initiating combat between them and the player if found
	def refresh(self):
		the_player = None
		for who in self.room_info['who_is_here']:
			if (type(who) == Player):
				the_player = who
		for who in self.room_info['who_is_here']:
			if ((who.getInfo('hostile') == True) and (the_player != None) and (the_player.getInfo('health') > 0)):
				print()
				this_fight = Combat(self.engine_instance, the_player, who)
				this_fight.otherAttack()
		return 0

#  name: Combat
#
#  Combat is an event where two Mobs fight one another.
class Combat:
	
	# # # #
	# Private Functions
	# # # #
	
	#  name: Combat.__init__
	#  @param engine_instance - VuurEngine
	#  @param this_player - Player
	#  @param defender - Mob or Questmaster
	#
	# Initializes a Combat object, which is an event between two Mobs
	def __init__(self, engine_instance, this_player, this_other):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		#
		self.this_player = this_player
		self.this_other = this_other
		#
		self.flee = False
		self.location = this_player.getInfo('location')
		self.location_neighbors = self.location.getInfo('neighbors')
		#
		self.this_other.setInfo('hostile', True)
		#
		self.combat_end = False
		self.combat_began = False
		#
		if ((self.this_other.getInfo('health') <= 0) or (self.this_player.getInfo('health') <= 0)):
			self.combat_end = True
		else:
			self.combat_began = True
			print("You enter into a fight with " + self.this_other.getInfo('name') + "...")
			print()


	#  name: Combat.endCombat()
	#  @return 0 - Mob, Player, or Defender
	#
	# Ends the Combat event
	def endCombat(self):
		if (self.combat_began == True):
			if ((self.this_other.getInfo('dead') == True) or (self.this_other.getInfo('health') <= 0)):
				print()
				print("You beat " + self.this_other.getInfo('name') + "...")
				#self.this_other.refresh()
			if (self.flee == True):
				print()
				print("You got away from " + self.this_other.getInfo('name') + "...")
				print()
			if ((self.this_player.getInfo('dead') == True) or (self.this_player.getInfo('health') <= 0)):
				print("You were beaten by " + self.this_other.getInfo('name') + "...")
				#self.this_player.playerRefresh()
		else:
			pass

		return 0
	
	#  name: Combat.fleeFromCombat()
	#  @return flee_location - Room or None
	#
	# Player flees from Combat
	def fleeFromCombat(self):
		flee_location = None
		flee_directions_raw = list(self.location_neighbors.keys())
		flee_directions = []
		for direction in flee_directions_raw:
			if (self.location_neighbors[direction] != None):
				flee_directions.append(direction)
		where_to_flee = random.choice(flee_directions)
		if (self.location_neighbors[where_to_flee] != None):
			flee_location = where_to_flee
			self.flee = True
		return flee_location
	
	#  name: Combat.playerAttack()
	#  @return 0 - Int
	#
	# The player's attack action
	def playerAttack(self):
		if ((self.this_player.getInfo('health') > 0) and (self.this_other.getInfo('health') > 0)):
			print("You (" + str(self.this_player.getInfo('health')) +"/100) -vs- " + self.this_other.getInfo('name') + " (" + str(self.this_other.getInfo('health')) + "/100)")
			#
			attack_or_flee = input("[A]ttack or [F]lee? ").lower()
			print()
			if ((attack_or_flee == "a") or (attack_or_flee == "attack")):
				attack_roll_chance = random.randint(1, 20)
				if (attack_roll_chance > 5):
					attack_roll = random.randint(1, self.this_player.getInfo('attack'))
					self.this_other.takeDamage(attack_roll)
					print("You dealt " + str(attack_roll) + " damage...")
				else:
					print("Your attack missed...")
				self.otherAttack()
				#
			elif ((attack_or_flee == "f") or (attack_or_flee == "flee")):
				flee_roll_chance = random.randint(1, 20)
				if (flee_roll_chance > 15):
					flee_location = self.fleeFromCombat()
					if (self.flee == True):
						self.endCombat()
						self.this_player.playerChangeRoom(flee_location)
					else:
						print("You were unable to flee...")
						self.otherAttack()
				else:
					print("You were unable to flee...")
					self.otherAttack()
			else:
				print("You stand there confused...")
				self.otherAttack()
			#
		elif (self.this_player.getInfo('health') <= 0):
			self.endCombat()
		return 0

	#  name: Combat.otherAttack()
	#  @return 0 - Int
	#
	# The other mob's attack action
	def otherAttack(self):
		if ((self.this_player.getInfo('health') > 0) and (self.this_other.getInfo('health') > 0)):
			#
			attack_roll_chance = random.randint(1, 20)
			#
			if (attack_roll_chance > 5):
				attack_roll = random.randint(1, self.this_other.getInfo('attack'))
				self.this_player.takeDamage(attack_roll)
				print("You took " + str(attack_roll) + " damage...")
			else:
				print("Their attack missed...")
			#
			self.playerAttack()
		elif (self.this_other.getInfo('health') <= 0):
			self.endCombat()
		return 0

#  name: Quest
#
#  A Quest is a task that the Player must complete by killing a
#  particular Mob, or finding a particular Item.
class Quest:
	
	# # # #
	# Public Functions
	# # # #

	#  name: Quest.__init__
	#  @param engine_instance - VuurEngine
	#  @param name - String
	#  @param fetch_or_kill - String ('fetch' or 'kill')
	#  @param quest_target - Item (if 'fetch_or_kill' == 'fetch') or Mob (if 'fetch_or_kill' == 'kill')
	#  @param quest_reward - Item or None
	#  @final_quest - Bool
	#
	# Initializes a Quest object
	def __init__(self, engine_instance, name, fetch_or_kill, quest_target, quest_reward, final_quest):
		# Associate this object with a particular instance of the VuurEngine
		self.engine_instance = engine_instance
		self.engine_instance.registerObject(self)
		#
		self.quest_info = {}
		self.quest_info['name'] = name
		self.quest_info['fetch_or_kill'] = fetch_or_kill
		self.quest_info['quest_target'] = quest_target
		self.quest_info['quest_reward'] = quest_reward
		self.quest_info['prereqs'] = []
		self.quest_info['questgiver'] = None
		self.quest_info['assigned_to'] = None
		self.quest_info['completed'] = False
		self.quest_info['final_quest'] = final_quest
		#
		self.quest_info['quest_description'] = None
		if (self.quest_info['fetch_or_kill'] == 'fetch'):
			self.quest_info['quest_description'] = "I need you to bring me the " + self.quest_info['quest_target'].getInfo('name')
		elif (self.quest_info['fetch_or_kill'] == 'kill'):
			self.quest_info['quest_description'] = "I need you to kill the " + self.quest_info['quest_target'].getInfo('name')
		#
		self.quest_info['quest_completion_text'] = None
		if (self.quest_info['fetch_or_kill'] == 'fetch'):
			self.quest_info['quest_completion_text'] = "Thank you for bringing me the " + self.quest_info['quest_target'].getInfo('name')
		elif (self.quest_info['fetch_or_kill'] == 'kill'):
			self.quest_info['quest_completion_text'] = "Thank you for killing the " + self.quest_info['quest_target'].getInfo('name')

	#  name: Quest.getInfo
	#  @param info - String
	#  @return to_return - *
	#
	# Returns requested info from 'quest_info' dictionary
	def getInfo(self, info):
		to_return = None
		if (info in self.quest_info):
			to_return = self.quest_info[info]
		return to_return

	#  name: Quest.setInfo
	#  @param info - String
	#  @param value - *
	#  @return 0 - Int
	#
	# Sets the value for key (determined by 'info' param) in the 'quest_info' dictionary
	def setInfo(self, info, value):
		if (info in self.quest_info):
			self.quest_info[info] = value
		return 0

	#  name: Quest.addPrereq
	#  @param prereq_quest - Quest
	#  @return 0 - Int
	#
	# Adds a prerequisite quest
	def addPrereq(self, prereq_quest):
		if (type(prereq_quest) == Quest):
			self.quest_info['prereqs'].append(prereq_quest)
		return 0

	# # # #
	# Private Functions
	# # # #

	#  name: Quest.checkCompletion
	#  @return 0 - Int
	#
	# Checks status of quest completion
	def checkCompletion(self):
		if (self.quest_info['fetch_or_kill'] == "fetch"):
			if (self.quest_info['quest_target'] in self.quest_info['questgiver'].getInfo('inventory')):
				self.quest_info['completed'] = True
		elif (self.quest_info['fetch_or_kill'] == "kill"):
			if (self.quest_info['quest_target'].getInfo('dead') == True):
				self.quest_info['completed'] = True
		return 0

	#  name: Quest.startQuest
	#  @param assign_quest_to - Player
	#  @return 0 - Int
	#
	# Assigns a quest to a player
	def startQuest(self, assign_quest_to):
		assign_quest_to.giveQuest(self)
		self.quest_info['assigned_to'] = assign_quest_to
		return 0

	#  name: Quest.endQuest
	#  @return 0 - Int
	#
	# Ends a quest
	def endQuest(self):
		if (self.quest_info['quest_reward'] != None):
			self.quest_info['assigned_to'].giveItem(self.quest_info['quest_reward'])
		self.quest_info['assigned_to'].finishQuest(self)
		return 0
