#!/usr/bin/python3

#  VuurEngine Custom Game Loader (vegame.py)
#
#  Copyright (c) 2023, Chris Blankenship <cblankenship@pm.me>
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import datetime,getpass,pathlib,random,re,string,sys,vuurengine

# Stores contents of loaded custom game file
loadfile_contents = ""

# Dictionaries to store Objects and Object metadata
game_dict = {}
game_objects_dict = {}

# Variables to store globally needed values (Engine and Player)
which_engine = None
which_player = None

def launch_game():
	# # # #
	#
	# Create the game interpreter
	# Params: Engine Instance (VuurEngine), Main Player (Player)
	this_interpreter = vuurengine.Interpreter(game_objects_dict[which_engine], game_objects_dict[which_player])
	#
	# Start the game console
	this_interpreter.gameConsole()
	#
	return 0

####

def create_engine(engine_key, engine_dict):
	#
	global which_engine
	#
	# Debug allowed?
	to_debug = False
	if ('debug' in engine_dict.keys()) and (engine_dict['debug'] == "True"):
		to_debug = True
	# # # #
	#
	# Initialize the engine instance
	# Params: Game Name (String), Copyright Year (String), Copyright Holder Name (String), Copyright Holder Email (String), Copyright Holder Website (String)
	this_engine = vuurengine.VuurEngine(engine_dict['name'], engine_dict['copyright year'], engine_dict['copyright holder name'], engine_dict['copyright holder email'], engine_dict['copyright holder website'], debug_allowed=to_debug)
	#
	# Store Engine object in objects dict
	game_objects_dict[engine_key] = this_engine
	#
	# Store engine identifier in applicable global variable
	which_engine = engine_key
	#
	return 0

def create_room(room_key, room_dict):
	#
	# Is the room locked?
	is_locked = False
	room_password = None
	if ('locked' in room_dict.keys()) and (room_dict['locked'] == "True"):
		is_locked = True
		if ('password' in room_dict.keys()) and (room_dict['password'] != "None"):
			room_password = room_dict['password']
	#
	# Is the room hidden?
	is_hidden = False
	if ('hidden' in room_dict.keys()) and (room_dict['hidden'] == "True"):
		is_hidden = True
	# # # #
	#
	# Create the Rooms
	#
	this_room = None
	# Params: Engine Instance (VuurEngine), Room Name (String), Locked (Bool), Hidden (Bool), Password (String or None)
	this_room = vuurengine.Room(game_objects_dict[which_engine], room_dict['name'], is_locked, is_hidden, password=room_password)
	#
	# Set additional info for room
	if 'description' in room_dict.keys():
		this_room.setInfo('description', room_dict['description'])
	#
	# Store Room object in objects dict
	game_objects_dict[room_key] = this_room
	#
	return 0

def link_rooms(room_key, room_dict):
	#
	to_link_north = None
	to_link_south = None
	to_link_east = None
	to_link_west = None
	to_link_up = None
	to_link_down = None
	#
	# Find which room is being described
	if ('north' in room_dict.keys()) and (room_dict['north'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == room_dict['north'].casefold():
					to_link_north = game_objects_dict[possible_room]
					break
	if ('south' in room_dict.keys()) and (room_dict['south'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == room_dict['south'].casefold():
					to_link_south = game_objects_dict[possible_room]
					break
	if ('east' in room_dict.keys()) and (room_dict['east'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == room_dict['east'].casefold():
					to_link_east = game_objects_dict[possible_room]
					break
	if ('west' in room_dict.keys()) and (room_dict['west'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == room_dict['west'].casefold():
					to_link_west = game_objects_dict[possible_room]
					break
	if ('up' in room_dict.keys()) and (room_dict['up'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == room_dict['up'].casefold():
					to_link_up = game_objects_dict[possible_room]
					break
	if ('down' in room_dict.keys()) and (room_dict['down'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == room_dict['down'].casefold():
					to_link_down = game_objects_dict[possible_room]
					break
	# # # # 
	#
	# Link the rooms together
	# Params: North (Room or None), South (Room or None), East (Room or None), West (Room or None), Up (Room or None), Down (Room or None)
	game_objects_dict[room_key].linkRoom(to_link_north, to_link_south, to_link_east, to_link_west, to_link_up, to_link_down)
	#
	return 0
	
def create_item(item_key, item_dict):
	#
	# Find Synonyms for Item
	synonym_list = []
	if ('synonyms' in item_dict.keys()) and (item_dict['synonyms'] != "None"):
		synonym_list = re.split(r'(?<!\\);', item_dict['synonyms'])
	#
	# # # #
	# 
	# Create items
	# Params: Engine Instance (VuurEngine), Item Name (String), Item Description (String)
	this_item = vuurengine.Item(game_objects_dict[which_engine], item_dict['name'], item_dict['description'], synonyms=synonym_list)
	#
	# Add writing to Item if applicable
	if ('writing' in item_dict.keys()) and (item_dict['writing'] != "None"):
		this_item.setWriting(item_dict['writing'])
	#
	# Store Item in a location
	if ('location' in item_dict.keys()) and (item_dict['location'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == item_dict['location'].casefold():
					game_objects_dict[possible_room].giveItem(this_item)
					break
	#
	# Store Room object in objects dict
	game_objects_dict[item_key] = this_item
	#
	return 0

def create_weapon(weapon_key, weapon_dict):
	#
	# Find Synonyms for Item
	synonym_list = []
	if ('synonyms' in weapon_dict.keys()) and (weapon_dict['synonyms'] != "None"):
		synonym_list = re.split(r'(?<!\\);', weapon_dict['synonyms'])
	#
	# # # #
	# 
	# Create weapon
	# Params: Engine Instance (VuurEngine), Weapon Name (String), Weapon Description (String), Attack (Int)
	this_weapon = vuurengine.Weapon(game_objects_dict[which_engine], weapon_dict['name'], weapon_dict['description'], int(weapon_dict['attack']), synonyms=synonym_list)
	#
	# Add writing to Weapon if applicable
	if ('writing' in weapon_dict.keys()) and (weapon_dict['writing'] != "None"):
		this_item.setWriting(weapon_dict['writing'])
	#
	# Store Item in a location
	if ('location' in weapon_dict.keys()) and (weapon_dict['location'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == weapon_dict['location'].casefold():
					game_objects_dict[possible_room].giveItem(this_weapon)
					break
	#
	# Store Room object in objects dict
	game_objects_dict[weapon_key] = this_weapon
	#
	return 0

def create_consumable(consumable_key, consumable_dict):
	#
	# Find Synonyms for Consumable
	synonym_list = []
	if ('synonyms' in consumable_dict.keys()) and (consumable_dict['synonyms'] != "None"):
		synonym_list = re.split(r'(?<!\\);', consumable_dict['synonyms'])
	#
	# # # #
	# 
	# Create consumable
	# Params: Engine Instance (VuurEngine), Consumable Name (String), Consumable Description (String), Attack Boost (Int), Health Boost (Int)
	this_consumable = vuurengine.Consumable(game_objects_dict[which_engine], consumable_dict['name'], consumable_dict['description'], int(consumable_dict['attack boost']), int(consumable_dict['health boost']), synonyms=synonym_list)
	#
	# Add writing to Consumable if applicable
	if ('writing' in consumable_dict.keys()) and (consumable_dict['writing'] != "None"):
		this_consumable.setWriting(consumable_dict['writing'])
	#
	# Store Consumable in a location
	if ('location' in consumable_dict.keys()) and (consumable_dict['location'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == consumable_dict['location'].casefold():
					game_objects_dict[possible_room].giveItem(this_consumable)
					break
	#
	# Store Room object in objects dict
	game_objects_dict[consumable_key] = this_consumable
	#
	return 0

def create_player(player_key, player_dict):
	#
	global which_player
	#
	# Find location
	player_location = None
	if ('location' in player_dict.keys()) and (player_dict['location'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == player_dict['location'].casefold():
					player_location = game_objects_dict[possible_room]
					break
	# # # #
	#
	# Create the main player
	# Params: Engine Instance (VuurEngine), Player Name (String), Location (Room)
	this_player = vuurengine.Player(game_objects_dict[which_engine], player_dict['name'], player_location)
	#
	# Add inventory
	player_inventory = []
	inventory_named_list = []
	if ('inventory' in player_dict.keys()) and (player_dict['inventory'] != "None"):
		inventory_named_list = re.split(r'(?<!\\);', player_dict['inventory'])
		for possible_item in game_dict:
			if (game_dict[possible_item]['type'].casefold() == 'item'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'weapon'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'consumable'.casefold()):
				for this_item in inventory_named_list:
					if game_dict[possible_item]['name'].casefold() in this_item.casefold():
						player_inventory.append(game_objects_dict[possible_item])
	if len(player_inventory) > 0:
		for inventory_item in player_inventory:
			this_player.giveItem(inventory_item)
	#
	# Add equipped weapon
	if ('equipped' in player_dict.keys()) and (player_dict['equipped'] != "None"):
		for possible_item in game_dict:
			if game_dict[possible_item]['type'].casefold() == 'weapon'.casefold():
				if game_dict[possible_item]['name'].casefold() == player_dict['equipped'].casefold():
					this_player.equip(game_objects_dict[possible_item])
					break
	#
	# Store Player object in the object dict
	game_objects_dict[player_key] = this_player
	#
	# Store Player identifier in applicable global variable
	which_player = player_key
	#
	#
	return 0

def create_mob(mob_key, mob_dict):
	#
	# Find Synonyms for Mob
	synonym_list = []
	if ('synonyms' in mob_dict.keys()) and (mob_dict['synonyms'] != "None"):
		synonym_list = re.split(r'(?<!\\);', mob_dict['synonyms'])
	#
	# Find location
	mob_location = None
	if ('location' in mob_dict.keys()) and (mob_dict['location'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == mob_dict['location'].casefold():
					mob_location = game_objects_dict[possible_room]
					break
	#
	# Is hostile?
	is_hostile = False
	if mob_dict['hostile'] == "True":
		is_hostile = True
	# # # #
	#
	# Create non-player mobs
	# Params: Engine Instance (VuurEngine), Mob Name (String), Location (Room), Hostime (Bool)
	this_mob = vuurengine.Mob(game_objects_dict[which_engine], mob_dict['name'], mob_location, is_hostile, synonyms=synonym_list)
	#
	# Add inventory
	mob_inventory = []
	inventory_named_list = []
	if ('inventory' in mob_dict.keys()) and (mob_dict['inventory'] != "None"):
		inventory_named_list = re.split(r'(?<!\\);', mob_dict['inventory'])
		for possible_item in game_dict:
			if (game_dict[possible_item]['type'].casefold() == 'item'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'weapon'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'consumable'.casefold()):
				for this_item in inventory_named_list:
					if game_dict[possible_item]['name'].casefold() in this_item.casefold():
						mob_inventory.append(game_objects_dict[possible_item])
	if len(mob_inventory) > 0:
		for inventory_item in mob_inventory:
			this_mob.giveItem(inventory_item)
	#
	# Add equipped weapon
	mob_equipped = None
	if ('equipped' in mob_dict.keys()) and (mob_dict['equipped'] != "None"):
		for possible_item in game_dict:
			if game_dict[possible_item]['type'].casefold() == 'weapon'.casefold():
				if game_dict[possible_item]['name'].casefold() == mob_dict['equipped'].casefold():
					this_player.equip(game_objects_dict[possible_item])
					break
	#
	# Add additional information
	if ('talking points' in mob_dict.keys()) and (mob_dict['talking points'] != "None"):
		talking_points_list = re.split(r'(?<!\\);', mob_dict['talking points'])
		for line in talking_points_list:
			this_mob.addTalkingPoint(line)
	#
	# Store Mob object in the object dict
	game_objects_dict[mob_key] = this_mob
	#
	return 0

def create_questmaster(questmaster_key, questmaster_dict):
	#
	# Find Synonyms for Questmaster
	synonym_list = []
	if ('synonyms' in questmaster_dict.keys()) and (questmaster_dict['synonyms'] != "None"):
		synonym_list = re.split(r'(?<!\\);', questmaster_dict['synonyms'])
	#
	# Find location
	questmaster_location = None
	if ('location' in questmaster_dict.keys()) and (questmaster_dict['location'] != "None"):
		for possible_room in game_dict:
			if game_dict[possible_room]['type'].casefold() == 'room'.casefold():
				if game_dict[possible_room]['name'].casefold() == questmaster_dict['location'].casefold():
					questmaster_location = game_objects_dict[possible_room]
					break
	# # # #
	#
	# Create questmaster
	# Params: Engine Instance (VuurEngine), Questmaster Name (String), Location (Room)
	this_questmaster = vuurengine.Questmaster(game_objects_dict[which_engine], questmaster_dict['name'], questmaster_location, synonyms=synonym_list)
	#
	# Add inventory
	questmaster_inventory = []
	inventory_named_list = []
	if ('inventory' in questmaster_dict.keys()) and (questmaster_dict['inventory'] != "None"):
		inventory_named_list = re.split(r'(?<!\\);', questmaster_dict['inventory'])
		for possible_item in game_dict:
			if (game_dict[possible_item]['type'].casefold() == 'item'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'weapon'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'consumable'.casefold()):
				for this_item in inventory_named_list:
					if game_dict[possible_item]['name'].casefold() in this_item.casefold():
						questmaster_inventory.append(game_objects_dict[possible_item])
	if len(questmaster_inventory) > 0:
		for inventory_item in questmaster_inventory:
			this_questmaster.giveItem(inventory_item)
	#
	# Add equipped weapon
	questmaster_equipped = None
	if ('equipped' in questmaster_dict.keys()) and (questmaster_dict['equipped'] != "None"):
		for possible_item in game_dict:
			if game_dict[possible_item]['type'].casefold() == 'weapon'.casefold():
				if game_dict[possible_item]['name'].casefold() == questmaster_dict['equipped'].casefold():
					this_player.equip(game_objects_dict[possible_item])
					break
	#
	# Add additional information
	if ('talking points' in questmaster_dict.keys()) and (questmaster_dict['talking points'] != "None"):
		talking_points_list = re.split(r'(?<!\\);', questmaster_dict['talking points'])
		for line in talking_points_list:
			this_questmaster.addTalkingPoint(line)
	#
	# Store Questmaster object in the object dict
	game_objects_dict[questmaster_key] = this_questmaster
	#
	return 0

def create_quest(quest_key, quest_dict):
	#
	# find Quest target
	this_target = None
	fetch_or_kill = None
	if quest_dict['fetch or kill'].casefold() == "fetch".casefold():
		fetch_or_kill = 'fetch'
		if ('target' in quest_dict.keys()) and (quest_dict['target'] != "None"):
			for possible_item in game_dict:
				if (game_dict[possible_item]['type'].casefold() == 'item'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'weapon'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'consumable'.casefold()):
					if game_dict[possible_item]['name'].casefold() == quest_dict['target'].casefold():
						this_target = game_objects_dict[possible_item]
						break
	elif quest_dict['fetch or kill'].casefold() == "kill".casefold():
		fetch_or_kill = 'kill'
		if ('target' in quest_dict.keys()) and (quest_dict['target'] != "None"):
			for possible_mob in game_dict:
				if (game_dict[possible_mob]['type'].casefold() == 'mob'.casefold()) or (game_dict[possible_mob]['type'].casefold() == 'questmaster'.casefold()):
					if game_dict[possible_mob]['name'].casefold() == quest_dict['target'].casefold():
						this_target = game_objects_dict[possible_mob]
						break
	#
	# Find Quest reward
	this_reward = None
	if ('reward' in quest_dict.keys()) and (quest_dict['reward'] != "None"):
		for possible_item in game_dict:
			if (game_dict[possible_item]['type'].casefold() == 'item'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'weapon'.casefold()) or (game_dict[possible_item]['type'].casefold() == 'consumable'.casefold()):
				if game_dict[possible_item]['name'].casefold() == quest_dict['reward'].casefold():
					this_reward = game_objects_dict[possible_item]
					break
	#
	# Is the Quest the final quest?
	is_final = False
	if quest_dict['final'] == "True":
		is_final = True
	#
	# # # #
	# Create the quests
	# Params: Engine Instance (VuurEngine), Fetch or Kill (String, "fetch" or "kill"), Target (Item if "fetch", Mob if "kill"), Reward (Item or None), Final Quest (Bool)
	#print(this_target.getInfo('name'))
	this_quest = vuurengine.Quest(game_objects_dict[which_engine], quest_dict['name'], fetch_or_kill, this_target, this_reward, is_final)
	#
	# Add Quest description and completion texts
	if ('description text' in quest_dict.keys()) and (quest_dict['description text'] != "None"):
		this_quest.setInfo("quest_description", quest_dict['description text'])
	if ('completion text' in quest_dict.keys()) and (quest_dict['completion text'] != "None"):
		this_quest.setInfo("quest_completion_text", quest_dict['completion text'])
	#
	# Assign Quest to Questmaster
	if ('given by' in quest_dict.keys()) and (quest_dict['given by'] != "None"):
		for possible_questmaster in game_dict:
			if game_dict[possible_questmaster]['type'].casefold() == 'questmaster'.casefold():
				if game_dict[possible_questmaster]['name'].casefold() == quest_dict['given by'].casefold():
					game_objects_dict[possible_questmaster].addQuestToGive(this_quest)
					break
	#
	# Store Quest object in the object dict
	game_objects_dict[quest_key] = this_quest
	#
	return 0

def prereq_quests(quest_key, quest_dict):
	#
	# Add prereq quests
	this_quest_prereqs = []
	prereq_name_list = []
	if ('prereqs' in quest_dict.keys()) and (quest_dict['prereqs'] != "None"):
		prereq_name_list = re.split(r'(?<!\\);', quest_dict['prereqs'])
		for possible_quest in game_dict:
			if game_dict[possible_quest]['type'].casefold() == 'quest'.casefold():
				for this_quest in prereq_name_list:
					if game_dict[possible_quest]['name'].casefold() == this_quest.casefold():
						this_quest_prereqs.append(game_objects_dict[possible_quest])
	if len(this_quest_prereqs) > 0:
		for that_quest in this_quest_prereqs:
			game_objects_dict[quest_key].addPrereq(that_quest)
	#
	return 0

####

def parse_game_dict():
	#
	# Step One, create the Engine object
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'engine'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error, Engine object requires 'Name' value")
				exit()
			if 'copyright year' not in game_dict[entry].keys():
				game_dict[entry]['copyright year'] = datetime.datetime.now().date().strftime("%Y")
			if 'copyright holder name' not in game_dict[entry].keys():
				game_dict[entry]['copyright holder name'] = getpass.getuser()
			if 'copyright holder email' not in game_dict[entry].keys():
				game_dict[entry]['copyright holder email'] = getpass.getuser() + "@localhost"
			if 'copyright holder website' not in game_dict[entry].keys():
				game_dict[entry]['copyright holder website'] = 'localhost'
			#
			# If everything's good, create the object
			if which_engine == None:
				create_engine(entry, game_dict[entry])
			else:
				print("Error: Only one Engine object can exist.")
				exit()
			#
			# Only one 'Engine' object should exist, break this loop after the first one is loaded
			break
	#
	# Step Two, create all Room objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'room'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Room object requires 'Name' value")
				exit()
			#
			# Check if name is unique
			for possible_room in game_dict:
				if ((game_dict[possible_room]['type'].casefold() == 'room'.casefold()) and (possible_room != entry)):
					if game_dict[possible_room]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the ovject
			create_room(entry, game_dict[entry])

				
	#
	# Step Three, link Rooms together
	for entry in game_dict:
		if (game_dict[entry]['type'].casefold() == 'room'.casefold()) and (entry in game_objects_dict):
			link_rooms(entry, game_dict[entry])
	#
	# Step Four, create the Item objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'item'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Item object requires 'Name' value")
				exit()
			if 'description' not in game_dict[entry].keys():
				print("Error: Item object requires 'Description' value")
				exit()
			#
			# Check if name is unique
			for possible_item in game_dict:
				if ((game_dict[possible_item]['type'].casefold() == 'item'.casefold()) and (possible_item != entry)):
					if game_dict[possible_item]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the object
			create_item(entry, game_dict[entry])

	#
	# Step Five, create Weapon objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'weapon'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Weapon object requires 'Name' value")
				exit()
			if 'description' not in game_dict[entry].keys():
				print("Error: Weapon object '" + game_dict[entry]['name'] + "' requires 'Description' value")
				exit()
			if 'attack' not in game_dict[entry].keys():
				print("Error: Weapon object requires 'Attack' value")
				exit()
			if 'attack' in game_dict[entry].keys():
				if bool(re.match("\-{0,1}[0-9]+", game_dict[entry]['attack'])) == False:
					print("Error: Weapon object '" + game_dict[entry]['name'] + "' requires 'Attack' to be a numeric value")
					exit()
			#
			# Check if name is unique
			for possible_weapon in game_dict:
				if ((game_dict[possible_weapon]['type'].casefold() == 'weapon'.casefold()) and (possible_weapon != entry)):
					if game_dict[possible_weapon]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the object
			create_weapon(entry, game_dict[entry])
	#
	# Step Six, create Consumable objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'consumable'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Consumable object requires 'Name' value")
				exit()
			if 'description' not in game_dict[entry].keys():
				print("Error: Consumable object '" + game_dict[entry]['name'] + "' requires 'Description' value")
				exit()
			#
			# Need either or both 'attack boost' or 'health boost'
			if ('attack boost' in game_dict[entry].keys()) and ('health boost' in game_dict[entry].keys()):
				if (bool(re.match("\-{0,1}[0-9]+", game_dict[entry]['attack boost'])) == False) or (bool(re.match("\-{0,1}[0-9]+", game_dict[entry]['health boost'])) == False):
					print("Error: Consumable object '" + game_dict[entry]['name'] + "' requires 'Attack Boost' and 'Health Boost' be numeric values")
					exit()
			elif ('attack boost' not in game_dict[entry].keys()) and ('health boost' in game_dict[entry].keys()):
				game_dict[entry]['attack boost'] = "0"
				if bool(re.match("\-{0,1}[0-9]+", game_dict[entry]['health boost'])) == False:
					print("Error: Consumable object '" + game_dict[entry]['name'] + "' requires 'Health Boost' be a numeric value")
					exit()
			elif ('attack boost' in game_dict[entry].keys()) and ('health boost' not in game_dict[entry].keys()):
				game_dict[entry]['health boost'] = "0"
				if bool(re.match("\-{0,1}[0-9]+", game_dict[entry]['attack boost'])) == False:
					print("Error: Consumable object '" + game_dict[entry]['name'] + "' requires 'Attack Boost' be a numeric value")
					exit()
			elif ('attack boost' not in game_dict[entry].keys()) and ('health boost' not in game_dict[entry].keys()):
				print("Error: Consumable object '" + game_dict[entry]['name'] + "' requires 'Attack Boost' and/or 'Health Boost' values")
				exit()
			#
			# Check if name is unique
			for possible_consumable in game_dict:
				if ((game_dict[possible_consumable]['type'].casefold() == 'consumable'.casefold()) and (possible_consumable != entry)):
					if game_dict[possible_consumable]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the object
			create_consumable(entry, game_dict[entry])
	#
	# Step Seven, create the Player object
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'player'.casefold():
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				game_dict[entry]['name'] = getpass.getuser()
			if 'location' not in game_dict[entry].keys():
				print("Error: Player object '" + game_dict[entry]['name'] + "' requires 'Location' value")
				exit()
			# If everything's good, create the object
			if which_player == None:
				create_player(entry, game_dict[entry])
			else:
				print("Error: Only one Player object can exist.")
				exit()
			#
			# Only one 'Player' object should exist, break this loop after the first one is loaded
			break
	#
	# Step Eight, create the Mob objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'mob'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Mob object '" + game_dict[entry]['name'] + "' requires 'Name' value")
				exit()
			if 'location' not in game_dict[entry].keys():
				print("Error: Mob object '" + game_dict[entry]['name'] + "' requires 'Location' value")
				exit()
			#
			# Check if name is unique
			for possible_mob in game_dict:
				if ((game_dict[possible_mob]['type'].casefold() == 'mob'.casefold()) and (possible_mob != entry)):
					if game_dict[possible_mob]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the object
			create_mob(entry, game_dict[entry])
	#
	# Step Nine, create the Questmaster objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'questmaster'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Questmaster object '" + game_dict[entry]['name'] + "' requires 'Name' value")
				exit()
			if 'location' not in game_dict[entry].keys():
				print("Error: Questmaster object '" + game_dict[entry]['name'] + "' requires 'Location' value")
				exit()
			#
			# Check if name is unique
			for possible_questmaster in game_dict:
				if ((game_dict[possible_questmaster]['type'].casefold() == 'questmaster'.casefold()) and (possible_questmaster != entry)):
					if game_dict[possible_questmaster]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the object
			create_questmaster(entry, game_dict[entry])
	#
	# Step Ten, create Quest objects
	for entry in game_dict:
		if game_dict[entry]['type'].casefold() == 'quest'.casefold():
			#
			# Check for required values
			if 'name' not in game_dict[entry].keys():
				print("Error: Quest object '" + game_dict[entry]['name'] + "' requires 'Name' value")
				exit()
			if 'fetch or kill' not in game_dict[entry].keys():
				print("Error: Quest object '" + game_dict[entry]['name'] + "' requires 'Fetch or Kill' value")
				exit()
			if 'target' not in game_dict[entry].keys():
				print("Error: Quest object '" + game_dict[entry]['name'] + "' requires 'Target' value")
				exit()
			if 'given by' not in game_dict[entry].keys():
				print("Error: Quest object '" + game_dict[entry]['name'] + "' requires 'Given By' value")
				exit()
			#
			# Check if name is unique
			for possible_quest in game_dict:
				if ((game_dict[possible_questmaster]['type'].casefold() == 'quest'.casefold()) and (possible_quest != entry)):
					if game_dict[possible_quest]['name'].casefold() == game_dict[entry]['name'].casefold():
						print("Error: 'Name' for object '" + game_dict[entry]['name'] + "' not unique")
						exit()
			#
			# If everything's good, create the object
			create_quest(entry, game_dict[entry])
	#
	# Step Eleven, add Quest prereqs
	for entry in game_dict:
		if (game_dict[entry]['type'].casefold() == 'quest'.casefold()) and (game_objects_dict[entry]):
			prereq_quests(entry, game_dict[entry])
	#
	return 0

def create_game_dict():
	#
	global game_dict
	#
	# Split Object Blocks using delimiter '===='
	loadfile_block = re.split("\n{0,1}\=\=\=\=\n", loadfile_contents)
	#
	# Iterate through file
	object_position = 1
	for this_object_block in loadfile_block:
		#
		# Create empty Dictionary
		this_object_dict = {}
		#
		# Iterate through Object Block
		for this_object_block_line in this_object_block.split("\n"):
			#
			# If line is not empty and matches expected input...
			if (this_object_block_line != '') and (this_object_block_line != None) and bool(re.match(".+\:.+", this_object_block_line)):
				#
				# Split the line
				this_object_block_line_split = this_object_block_line.split(':')
				#
				# Find the key
				this_object_key = None
				if (this_object_block_line_split[0].lstrip().lower() != '') and (this_object_block_line_split[0].lstrip().lower() != None):
					this_object_key = this_object_block_line_split[0].lstrip().lower()
				else:
					print("Error: Variable for object #" + str(object_position) + " appears to be empty")
					exit()
				#
				# Find the value
				if (this_object_block_line_split[1].lstrip() != '') and (this_object_block_line_split[1].lstrip() != None):
					this_object_value = this_object_block_line_split[1].lstrip()
				else:
					print("Error: Value for variable '" + this_object_key + "' in object #" + str(object_position) + " appears to be empty")
					exit()
				# And add it to the Dictionary
				this_object_dict[this_object_key] = this_object_value
		# If the dictionary is not empty, add it to the full game dictionary (game_dict) 
		if len(this_object_dict) > 0:
			#
			# Get type of the object
			this_object_dict_type = ''
			try:
				this_object_dict_type = this_object_dict['type']
			except:
				print("Error: Object #" + str(object_position) + " has no 'Type'")
				exit()
			sanitized_todt_array = []
			sanitized_todt_string = ""
			for this_char in this_object_dict_type:
				if this_char.isalnum():
					sanitized_todt_array.append(this_char.lower())
			if (len(sanitized_todt_array) > 0) and (sanitized_todt_array[0] in 'abcdefghijklmnopqrstuvwxyz'):
				sanitized_todt_string = ''.join(sanitized_todt_array)
			elif (len(sanitized_todt_array) > 0) and (sanitized_todt_array[0] not in 'abcdefghijklmnopqrstuvwxyz'):
				sanitized_todt_array.insert(0, random.choice('abcdefghijklmnopqrstuvwxyz'))
				sanitized_todt_string = ''.join(sanitized_todt_array)
			else:
				sanitized_todn_string = ''.join(random.choice('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') for i in range(5))
			#
			# Get name of the object
			this_object_dict_name = ""
			try:
				this_object_dict_name = this_object_dict['name']
			except:
				print("Error: Object #" + str(object_position) + " ('Type': " + this_object_dict_type + ") has no 'Name'")
				exit() 
			sanitized_todn_array = []
			sanitized_todn_string = ""
			for this_char in this_object_dict_name:
				if this_char.isalnum():
					sanitized_todn_array.append(this_char.lower())
			if (len(sanitized_todn_array) > 0) and (sanitized_todn_array[0] in 'abcdefghijklmnopqrstuvwxyz'):
				sanitized_todn_string = ''.join(sanitized_todn_array)
			elif (len(sanitized_todn_array) > 0) and (sanitized_todn_array[0] not in 'abcdefghijklmnopqrstuvwxyz'):
				sanitized_todn_array.insert(0, random.choice('abcdefghijklmnopqrstuvwxyz'))
				sanitized_todn_string = ''.join(sanitized_todn_array)
			else:
				sanitized_todn_string = ''.join(random.choice('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') for i in range(5))
			#
			# Store this_object_dict in game_dict with a unique key
			this_game_dict_key = sanitized_todt_string + "_" + sanitized_todn_string + "_" + ''.join(random.choice('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') for i in range(5))
			game_dict[this_game_dict_key] = this_object_dict
			#
			object_position += 1
	#
	return 0

def parse_file():
	#
	global loadfile_contents
	#
	# Load file should be the only argument passed
	file_to_load = sys.argv[1]
	#
	# Load file should be a ".veload" file
	if bool(re.match("^.+\.vegame$", file_to_load)):
		#
		# Check if load file actually exists, if it does...
		if pathlib.Path(file_to_load).is_file():
			loadfile_raw = open(file_to_load, 'r')
			loadfile_sanitized_list = []
			loadfile_sanitized_string = ""
			#
			# Go through each line
			for this_line in loadfile_raw:
				# 
				# Remove comments
				if bool(re.match("^\#.*$", this_line)):
					pass
				else:
					loadfile_sanitized_list.append(this_line)
			#
			# And as long as there is content, load it in
			if len(loadfile_sanitized_list) > 0:
				loadfile_contents = ''.join(loadfile_sanitized_list)
			else:
				print("Error: '" + file_to_load + "' appears to be empty")
				exit()
		#
		# If it doesn't, throw error
		else:
			print("Error: Cannot find " + file_to_load)
			exit()
	else:
		print("Error: " + file_to_load + " does not appear to be the correct file type")
		exit()
	#
	return 0

def main():
	parse_file()
	create_game_dict()
	parse_game_dict()
	launch_game()
	return 0

main()
