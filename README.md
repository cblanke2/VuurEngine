# VuurEngine

A homebrew text adventure engine written in python3

[[_TOC_]]

## Playing a game

### Starting a game

Depending on _how_ the game was written, it can be run in a number of ways.

If if was [written as a flat text file](#using-a-vegame-file), you can load it using the `vegame.py` script.

```
python3 vegame.py /path/to/custom_game.vegame
```

However, if it was [written as a python script](#using-python3), you can just run the python script directly.

```
python3 /path/to/custom_game.py
```

### Commands

The game interpreter can accept a number of commands that tell your player what to do, but they do need to be in a specific format. They are are as follows:

- `attack [Mob]`, `fight [Mob]`, `kill [Mob]` → Attack a Mob
- `drop [Item]`, `toss [Item]` → Remove an Item from your inventory and leave it in the Room you're currentrly in
- `equip [Weapon]`, `wield [Weapon]` → Equip an Item (specifically a Weapon) from your inventory
- `equipped` → Print out what Weapon you have equipped
- `examine [Item]`, `check [Item]` → Print the description of an Item/Weapon/Consumable
- `exit`, `quit`, `close`, `clear` → Quit the game
- `give [Item] to [Mob]`, `hand [Item] to [Mob]` → Remove an Item from your inventory and place it in the inventory of another Mob
- `health`, `stats`, `turns` → Print out your Player stats, including health, attack power, and how many turns have passed
- `help` → Print out help information for the game
- `info` → Print out general information about the game
- `inventory` → Print out your inventory
- `look` → Print the description of the Room you're currently in
- `move [Direction]`, `go [Direction]`, `[Direction]` → Go to the next Room in the specified direction
- `questlog`, `quests` → Print out the list of every Quest you have accepted but not completed
- `read [Item]` → Read what is written on an Item
- `take [Item]`, `grab [Item]`, `pick up [Item]`, `get [Item]` → Take an Item from the Room you're currentrly in and place it in your inventory and leave it in 
- `talk with [Mob]`, `talk to [Mob]`, `speak with [Mob]`, `speak to [Mob]` → Talk with another Mob
- `unequip` → Unequip what Weapon you currently have equipped
- `use [Consumable]`, `consume [Consumable]` → Use (and with it, destroy) an Item (specifically a Consumable) in your inventory

### Playing online

The VuurEngine is **not** designed to write multiplayer games, but that doesn't mean it can't be played remotely. You can [set up a server](#setting-up-a-server) and then access it via a terminal using something like [netcat](https://en.wikipedia.org/wiki/Netcat).

```
nc $SERVER_NAME_OR_IP $PORT_NUMBER
```

For example, I have a server running the example game on port **63132**, and it can be accessed with this command:

```
nc dockerengine.chrisblankenship.cloud 64132
```

## Writing a game

### Using a `.vegame` file

A VuurEngine game can be written in a flat text file with the `.vegame` extension. An example of this method can be found in the `examplegame_plaintext.vegame` file.

Every object you intend to use in the game must be described in this file, and must be delimited by the "====" string. The `vegame.py` program will handle creating all objects in the correct order.

Comments can be added to the file using the standard `#` char at the beginning of a string. These lines will not be interpreted by the `vegame.py` program.

At least three objects need to be created for a game to run. These are an `Engine` object, a `Player` object, and a `Location` object (the latter is a dependency for the `Player` obeject). This is _bare minimum_ by the way, so you won't actually be able to do anything unless you add more.

Each object you create must have a specified `Type` and a `Name`. `Names` cannot be re-used for objects of the same `Type`. For a few object types, these two variables will be all that are required. For (most) others, you'll need to specify more. Some object types will even have optional variables; these are either entirely optional for the game engine itself, or will be filled in with default values.

Some variables, such as the `Talking Points` for `Mob`s and `Questmaster`s, can be _overloaded_ with multiple values. These are to be delimited by the ";" char. If you need to use this char in the value for the variable (for some reason), it can be escaped ("\\;").

To run a game written as a `.vegame` file, you will need to pass it as an argument to the `vegame.py` program.

```
python3 vegame.py /path/to/custom_game.vegame
```

##### Engine

Required Variables:
```
Type: Engine
Name: $NAME_OF_THIS_GAME
```

Optional Variables:
```
Copyright Year: $COPYRIGHT_YEAR
Copyright Holder Name: $NAME_OF_COPYRIGHT_HOLDER
Copyright Holder Email: $EMAIL_OF_COPYRIGHT_HOLDER
Copyright Holder Website: $WEBSITE_OF_COPYRIGHT_HOLDER
Debug: [True|False]
```

`Copyright Year` will default to the current year if not specified.

`Copyright Holder Name` will default to the local username if not specified.

`Copyright Holder Email` will default to the local username @ "localhost" if not specified.

`Copyright Holder Website` will default to "localhost".

`Debug` will default to "False". "True" will allow players to access a debug console (which is more-or-less a full python prompt).

##### Room

Required Variables:
```
Type: Room
Name: $NAME_OF_THIS_ROOM
```

Optional Variables:
```
Locked: [True|False]
Password: $SOME_STRING
Hidden: [True|False]
North: $NAME_OF_ANOTHER_ROOM
South: $NAME_OF_ANOTHER_ROOM
East: $NAME_OF_ANOTHER_ROOM
West: $NAME_OF_ANOTHER_ROOM
Up: $NAME_OF_ANOTHER_ROOM
Down: $NAME_OF_ANOTHER_ROOM
```

`Locked` must be "True" or "False", but will otherwise default to "False" if not specified. This will determine if the _Room_ needs a password to be accessed. 

`Password` must be a string of text, but will otherwise be randomly generated. This will only be needed if `Locked` is "True".

`Hidden` must be "True" or "False", but will otherwise default to "False" if not specified. This will determine if the _Room_ appears to the _Player_ when looking. On visiting a _Room_, `Hidden` will be set to "False".

`North` must the the name of another _Room_ object, but will default to "None" if not specified. This is the _Room_ that's to the north of this _Room_.

`South` must the the name of another _Room_ object, but will default to "None" if not specified. This is the _Room_ that's to the south of this _Room_.

`East` must the the name of another _Room_ object, but will default to "None" if not specified. This is the _Room_ that's to the east of this _Room_.

`West` must the the name of another _Room_ object, but will default to "None" if not specified. This is the _Room_ that's to the west of this _Room_.

`Up` must the the name of another _Room_ object, but will default to "None" if not specified. This is the _Room_ that's above this _Room_.

`Down` must the the name of another _Room_ object, but will default to "None" if not specified. This is the _Room_ that's below this _Room_.

##### Player

Required Variables:
```
Type: Player
Name: $NAME_OF_THIS_PLAYER
Location: $NAME_OF_A_ROOM
```

`Location` must be the name of a _Room_.

Optional Variables:
```
Inventory: $NAME_OF_AN_ITEM;$NAME_OF_AN_ITEM
```
`Inventory` must be the names of one or more *Item*s, with multiple values separated by the ";" char, but will otherwise default to "None" if not specified.

An _Item_/_Consumable_/_Weapon_ cannot be in the inventory of a _Mob_/_Player_/_Questmaster_ if it has a Location set, and vice versa.

##### Mob

Required Variables:
```
Type: Mob
Name: $NAME_OF_THIS_MOB
Location: $NAME_OF_A_ROOM
```

`Location` must be the name of a _Room_.

Optional Variables:
```
Hostile: [True|False]
Inventory: $NAME_OF_AN_ITEM;$NAME_OF_AN_ITEM
Talking Points: $SOME_PHRASE;$SOME_PHRASE
Synonyms: $SOME_WORD;$SOME_WORD
```

`Hostile` must be "True" or "False", but will otherwise default to "False" if not specified. Detemines if a particular Mob is automatically hostile to the Player.

`Inventory` must be the names of one or more *Item*s, with multiple values separated by the `;` char, but will otherwise default to "None" if not specified.

`Talking Points` must be one or more strings of text, with multiple values separated by the `;` char, but will otherwise default to "None" if not specified. These are the phrases printed out when talking to a Mob.

`Synonyms` must be one or more strings of text with, multiple values separated by the `;` char. These are other words used to reference a Mob in commands.

An _Item_/_Consumable_/_Weapon_ cannot be in the inventory of a _Mob_/_Player_/_Questmaster_ if it has a Location set, and vice versa.

##### Item

Required Variables:
```
Type: Item
Name: $NAME_OF_THIS_ITEM
```

Optional Variables:
```
Location: $NAME_OF_A_ROOM
Writing: $SOME_STRING
Synonyms: $SOME_WORD;$SOME_WORD
```

`Location` must be the name of a _Room_, but will otherwise default to "None" if not specified.

`Writing` must be a string of text, but will otherwise default to "None" if not specified. This is what is written on particualr Item.

`Synonyms` must be one or more strings of text with, multiple values separated by the `;` char. These are other words used to reference an Item in commands.

An _Item_/_Consumable_/_Weapon_ cannot have a Location set if it's already in the inventory of a _Mob_/_Player_/_Questmaster_, and vice versa

##### Consumable

Required Variables:
```
Type: Consumable
Name: $NAME_OF_THIS_CONSUMABLE
```

At least one (or both) of these variables is required:
```
Attack Boost: $SOME_NUMBER
Health Boost: $SOME_NUMBER.
```

`Attack Boost` must be a numeric value. It can be negative!

`Health Boost` must be a numeric value. This can also be negative!

Optional Variables:
```
Location: $NAME_OF_A_ROOM
Writing: $SOME_STRING
Synonyms: $SOME_WORD;$SOME_WORD
```
`Location` must be the name of a _Room_, but will otherwise default to "None" if not specified.

`Writing` must be a string of text, but will otherwise default to "None" if not specified. This is what is written on particualr Consumable.

`Synonyms` must be one or more strings of text with, multiple values separated by the `;` char. These are other words used to reference a Consumable in commands.

An _Item_/_Consumable_/_Weapon_ cannot have a Location set if it's already in the inventory of a _Mob_/_Player_/_Questmaster_, and vice versa.

##### Weapon

Required Variables:
```
Type: Weapon
Name: $NAME_OF_THIS_WEAPON
Attack: $SOME_NUMBER
```

Optional Variables:
```
Location: $NAME_OF_A_ROOM
Writing: $SOME_STRING
Synonyms: $SOME_WORD;$SOME_WORD
```

`Location` must be the name of a _Room_, but will otherwise default to "None" if not specified.

`Writing` must be a string of text, but will otherwise default to "None" if not specified. This is what is written on particualr Weapon.

`Synonyms` must be one or more strings of text with, multiple values separated by the `;` char. These are other words used to reference a Weapon in commands.

An _Item_/_Consumable_/_Weapon_ cannot have a Location set if it's already in the inventory of a _Mob_/_Player_/_Questmaster_, and vice versa.

##### Questmaster

Required Variables:
```
Type: Questmaster
Name: $NAME_OF_THIS_QUESTMASTER
Location: $NAME_OF_A_ROOM
```

`Attack` must be a numeric value.

Optional Variables:
```
Inventory: $NAME_OF_AN_ITEM;$NAME_OF_AN_ITEM
Talking Points: $SOME_PHRASE;$SOME_PHRASE
Synonyms: $SOME_WORD;$SOME_WORD
```

`Inventory` must be the names of one or more *Item*s, with multiple values separated by the `;` char, but will otherwise default to "None" if not specified.

`Talking Points` must be one or more strings of texts, with multiple values separated by the `;` char, but will otherwise default to "None" if not specified. These are the phrases printed out when talking to a Mob.

`Synonyms` must be one or more strings of text with, multiple values separated by the `;` char. These are other words used to reference a Questmaster in commands.

An _Item_/_Consumable_/_Weapon_ cannot be in the inventory of a _Mob_/_Player_/_Questmaster_ if it has a Location set, and vice versa.

##### Quest

Required Variables:
```
Type: Quest
Name: $NAME_OF_THIS_QUEST
Fetch Or Kill: [Fetch|Kill]
Target: [$NAME_OF_MOB|$NAME_OF_ITEM]
Given By: $NAME_OF_A_QUESTMASTER
```

`Fetch Or Kill` must be either "Fetch" or "Kill".

`Target` must be the name of a _Mob_/_Questmaster_ (if `Fetch Or Kill` is "Fetch") or _Item_/_Consumable_/_Weapon_ (if `Fetch Or Kill` is "Fetch").
 
`Given By` must be the name of a _Questmaster_.


Optional Variables:
```
Reward: $NAME_OF_AN_ITEM
Final: [True|False]
Prereqs: $NAME_OF_A_QUEST;$NAME_OF_A_QUEST
Description Text: $SOME_STRING
Completion Text: $SOME_STRING
```

`Reward` must be the name of an _Item_, but will otherwise default to "None" if not specified. Any _Item_ used here may not already be in the inventory of a _Player_/_Mob_/_Questmaster_ or _Room_.

`Final` must be "True" or "False", but will otherwise default to "False" if not specified. This determines if the _Quest_ is the Final Quest of the game.

`Prereqs` must be the names of one or more *Quest*s, with multiple values separated by the `;` char, but will otherwise default to "None" if not specified. These are the *Quest*s that need to  be finished before this *Quest* can be taken.

`Description Text` must be a string of text, but will otherwise default to "None" if not specified.

`Completion Text` must be a string of text, but will otherwise default to "None" if not specified.

### Using python3

A VuurEngine game can also be written as a python3 script that uses the VuurEngine like any other python library. An example of this method can be found in the `examplegame_python.py` file.

While this method offers more control and ability to customize, it's also much easier to mess up here. Objects need to be created in a specific order for everything to work. The `vegame.py` program handles all of this for you when loading in a flat text file with the `.vegame` extension

To run a game written in pure python, you just need to run it as any other python3 program.

```
python3 /path/to/custom_game.py
```

##### VuurEngine

The `VuurEngine` object is the first object that needs to be created. All other objects will reference it, and only one can exist.

The `VuurEngine` object has several required params, in order these are (in order) as follows:

- `game_title` (String) → The title of your game
- `game_copyright_year` (String) → The copyright year of your game
- `game_copyright_holder` (String) → The copyright holder of your game
- `game_copyright_email` (String) → The email address of the copyright holder
- `game_copyright_website_string` (String) → The website of the game and/or copyright holder

```
# Game Name (String), Copyright Year (String), Copyright Holder Name (String), Copyright Holder Email (String), Copyright Holder Website (String)
your_game_engine = vuurengine.VuurEngine("Name Of Your Game", "2023", "Your Name", "yourname@email.tld", "www.yoursite.tld")
```

Optional params can also be passed to the `VuurEngine` object; these include (in no particular order):

- `game_contributors_title` (String) → What to call your game's contributors
- `game_contributors` (Dict) → A dictionary containing who contributed to your game
- `debug_allowed` (Bool) → Whether or not to allow the debug console


```
gc_title = "Contributors"
gc_dict= {
	"Nestor Makhno" : "Beta Testing"
	"Pierre-Joseph Proudhon" : "Programming"
	"Max Stirner" : "Story Writing"
	"Benjamin Tucker" : "Translation"
}

# Game Name (String), Copyright Year (String), Copyright Holder Name (String), Copyright Holder Email (String), Copyright Holder Website (String), Game Contributors Title (String), Game Contributors (Dict), Debug (Bool) 
your_game_engine = vuurengine.VuurEngine("Name Of Your Game", "2023", "Your Name", "yourname@email.tld", "www.yoursite.tld", game_contributors_title=gc_title, game_contributors=gc_dict debug_allowed=True)
```

##### Room

A `Room` object represents a location within the game.

At least one `Room` object needs to exist for your game to function. It requires several params; these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the `Room`
- `locked` (Bool) → If the `Room` should be locked
- `hidden` (Bool) → If the `Room` should be hidden

```
# Engine Instance (VuurEngine), Room Name (String), Locked (Bool), Hidden (Bool)
this_room = vuurengine.Room(your_game_engine, "Name of Room", False, False)
```

If the `locked` param for a `Room` object is set to **False**, a random password will be generated for it. If you'd like to set a specific password, you can pass that as an optional param.

- `password` (String) → If the `Room` is locked, this is the password to unlock it

```
# Engine Instance (VuurEngine), Room Name (String), Locked (Bool), Hidden (Bool), Password (String)
that_room = vuurengine.Room(your_game_engine, "Name of another Room", True, False, password="OhHiMark")
```

Once all the `Room`s you want are created, you will need to link them together to allow movement between them. This is done through the object's `linkRoom` function. It takes several params; these are (in order) as follows:

- `north` (Room or None) → The `Room` to the north of this `Room`
- `south` (Room or None) → The `Room` to the south of this `Room`
- `east` (Room or None) → The `Room` to the east of this `Room`
- `west` (Room or None) → The `Room` to the west of this `Room`
- `up` (Room or None) → The `Room` above this `Room`
- `down` (Room or None) → The `Room` below this `Room`


```
# North (Room or None), South (Room or None), East (Room or None), West (Room or None), Up (Room or None), Down (Room or None)
this_room.linkRoom(that_room, None, None, None, None, None)
that_room.linkRoom(None, this_room, None, None, None, None, None)
```

All `Room` objects have a dictionary, called `room_info`, that stores information about the Mob. The values stored in the dictionary are as follows: 

- `name` (String) → The name of the `Room`
- `description` (String) → A description of the `Room`
- `locked` (Bool) → Is the `Room` locked?
- `password` (String) → The password to unlock the `Room` if it is locked
- `hidden` (Bool) → Is the `Room` hidden?
- `inventory` (List of Items, Consumeables, and Weapons) → The `Item`s, `Consumeable`s, and `Weapon`s located in the `Room`
- `who_is_here` (List of Mobs) → The `Mob`s that are currently in the `Room`
- `neighbors` (Dict of Rooms) → A Dictionary that stores the neighbor `Room`s as set with the `linkRoom()` function

It's a good idea to let the game handle changing/setting these values on its own for a `Room` (except the neighbors, which can be set), but if you need to adjust them for whatever reason (such as storing an `Item` in a `Room`) or check a value, you can use the following fuctions.

The `getInfo()` function will return a value from the `room_info` dictionary. Just pass the name of the entry in the `room_info` dictionary.
```
# entry_to_request (String)
getInfo(entry_to_request):
```

The `setInfo()` function will set the value for a particualr entry in the `room_info` dictionary.
```
# entry_to_set (String), value_to_set (???)
setInfo(entry_to_set, value_to_set):
```

The `giveItem()` function will store an `Item`, `Consumable`, or `Weapon` in the `Room`.
```
# entry_to_request (String), item_to_give (Item, Consumable, or Weapon)
giveItem(item_to_give):
```

##### Player

The `Player` object is the character which the IRL player will control. It is also a required object for the game to run, and only one can exist. It inherits from the `Mob` object, but requires fewer params to be passed; in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the `Player`
- `location` (Room) → The initial location of the `Player`

```
# Engine Instance (VuurEngine), Name (String), Location (Room)
your_player = vuurengine.Player(your_game_engine, "Lysander Spooner", this_room)
```

##### Mob

A `Mob` object represents an NPC in the game. The `Player` and `Questmaster` objects inherit from it.

A `Mob` object has several required params, in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the `Mob`
- `location` (Room) → The initial location of the `Mob`
- `hostile` (Bool) → If the `Mob` should be hostile to the `Player` or not

```
# Engine Instance (VuurEngine), Name (String), Location (Room), Hostile (Bool)
chrisr_mob = vuurengine.Player(your_game_engine, "Chris-R", that_room, True)
```

An optional param can also be passed to a `Mob` object:
- `synonyms` (List of Strings) → Other words or phrases that the `Mob` can be referenced by

```
# Engine Instance (VuurEngine), Name (String), Location (Room), Hostile (Bool), Synonyms (List or None)
denny_synonyms = ['Neighbor Kid', 'Denny Boy']
denny_mob = vuurengine.Player(your_game_engine, "Denny", that_room, False, synonyms=denny_synonyms)
```

All `Mob` objects (as well as objects that inherit from the `Mob` class, like `Player` and `Questmaster`) have a dictionary, called `mob_info`, that stores information about the `Mob`. The values stored in the dictionary are as follows: 

- `name` (String) → The name of the `Mob`
- `location` (Room) → The `Room` the `Mob` is currently in
- `hostile` (Bool) → If the `Mob` should be hostile to the `Player` or not
- `health` (Int) → The `Mob`'s health (if it reaches 0, the mob is dead)
- `dead` (Bool) → Is the `Mob` is dead or not
- `attack` (Int) → The `Mob`'s attack power
- `talking_points` (List of Strings) → A list of strings that will print if/when the `Mob` is "spoken" to
- `inventory` (List of `Item`s, `Weapon`s, and `Consumable`s)
- `equipped` (Weapon) → A `Weapon` the Mob has equipped
- `visited_rooms` (List of Rooms) → A list of all of the `Room`s the `Mob` has visited
- `synonyms` (List of Strings) → Other words or phrases that the `Mob` can be referenced by

It's a good idea to let the game handle changing/setting these values on its own for a `Mob`, but if you need to adjust them for whatever reason (such as giving a `Mob` a default `Weapon`) or check a value, you can use the following fuctions.

The `getInfo()` function will return a value from the `mob_info` dictionary. Just pass the name of the entry in the `mob_info` dictionary.
```
# entry_to_request (String)
getInfo(entry_to_request):
```

The `setInfo()` function will set the value for a particualr entry in the `mob_info` dictionary.
```
# entry_to_set (String), value_to_set (???)
setInfo(entry_to_set, value_to_set):
```

The `equip()` function will have the Mob equip the passed Weapon.
```
# item_to_equip (Weapon)
equip(item_to_equip):
```
The `addTalkingPoints()` function will add the passed String to the list of phrases the Mob will say when spoken to.
```
# phrase (String)
addTalkingPoint(phrase):
```
The `giveItem()` function will add the passed Item, Weapon, or Consumable to the Mob's inventory.
```
# item_to_give (Item, Weapon, or Consumable)
giveItem(item_to_give):
```

##### Item

An `Item` object represents an item in the game. The `Consumable` and `Weapon` objects inherit from it.

A `Item` object has several required params, in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the `Item`
- `description` (String) → A description of the `Item`

```
# Engine Instance (VuurEngine), Name (String), Description (String)
lisas_dress = vuurengine.Item(your_game_engine, "Lisa's Dress", "A dress for Lisa to wear")
```

An optional param can also be passed to a `Item` object:
- `synonyms` (List of Strings) → Other words or phrases that the `Item` can be referenced by

```
# Engine Instance (VuurEngine), Name (String), Description (String), Synonyms (List or None)
spoon_synonyms = ['Picture', 'Picture Frame']
spoon_item = vuurengine.Item(your_game_engine, "Spoon Picture", "A framed picture of a spoon", synonyms=spoon_synonyms)
```

All `Item` objects (as well as objects that inherit from the `Item` class, like `Consumable` and `Weapon`) have a dictionary, called `item_info`, that stores information about the `Item`. The values stored in the dictionary are as follows: 

- `name` (String) → The name of the Mob
- `description` (String) → A description of the `Item`
- `can_equip` (Bool) → Can the `Item` be equipped (defaults to `False` and should stay this way, escept for `Weapon`s)
- `can_use` (Bool) → Can the `Item` be used (defaults to `False` and should stay this way, escept for `Consumeable`s)
- `writing` (String) → A string written on the `Item`
- `synonyms` (List of Strings) → Other words or phrases that the `Item` can be referenced by

It's a good idea to let the game handle changing/setting these values on its own for a `Item`, but if you need to adjust them for whatever reason (such as to set what is written on an `Item`) or check a value, you can use the following fuctions.

The `getInfo()` function will return a value from the `item_info` dictionary. Just pass the name of the entry in the `item_info` dictionary.
```
# entry_to_request (String)
getInfo(entry_to_request):
```

The `setInfo()` function will set the value for a particualr entry in the `item_info` dictionary.
```
# entry_to_set (String), value_to_set (???)
setInfo(entry_to_set, value_to_set):
```

The `setWriting()` function will add writing to an `Item`.
```
# writing (String)
setWriting(writing):
```
The `read_writing()` function will return what is written on the `Item`.
```
# writingString
readItem(writing):
```

##### Consumable

A `Consumeable` object is a type of `Item` that gives some benefit (or drawback), and is "destroyed" after use. It inherits from the `Item` object, and shares all of the params and functions of an `Item`, but additional params do need to be passed; in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the Item
- `description` (String) → A description of the item
- `attack_buff` (Int) → How much to (temporarily) buff a `Mob`'s attack value by
- `health_buff` (Int) → How much to (temporarily) buff a `Mob`'s health value by

```
# Engine Instance (VuurEngine), Name (String), Description (String), Attack Buff (Int), Health Buff (Int)
coffee_consumeable = vuurengine.Consumable(your_game_engine, "Coffee", "A nice hot coffee", 5, 10)
```

##### Weapon

A `Weapon` object is a type of `Item` that can be equipped and provides a bonus to attack power. It inherits from the `Item` object, and shares all of the params and functions of an `Item`, but additional params do need to be passed; in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the Item
- `description` (String) → A description of the item
- `attack` (Int) → How much to buff a `Mob`'s attack value by while the weapon is equipped

```
# Engine Instance (VuurEngine), Name (String), Description (String), Attack (Int)
handgun_weapon = vuurengine.Weapon(your_game_engine, "Gun", "For when Lisa betrays you", 20)
```

##### Questmaster

A `Questmaster` object represents a special type of NPC that can give `Quests`. It inherits from the `Mob` object, but requires fewer params to be passed; in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the `Questmaster`
- `location` (Room) → The location of the `Questmaster`

```
# Engine Instance (VuurEngine), Name (String), Location (Room)
emma_questmaster = vuurengine.Questmaster(your_game_engine, "Emma Goldman", that_room)
```
`Questmsters` do have an additional function that `Mob`s and other `Mob` derived objects don't have. That being, the `addQuestToGive` function, which allows a `Questmaster` to give `Quest`s to the `Player`. Just pass a quest object to it.

```
# quest_to_add (Quest)
addQuestToGive(quest_to_add):
```


##### Quest

A `Quest` object represents a task that the `Player` must do.

A `Quest` object has several required params, in order these are (in order) as follows:

- `engine_instance` (VuurEngine) → The `VuurEngine` object you previously created
- `name` (String) → The name of the `Quest`
- `fetch_ot_kill` (String, specifically "fetch" or "kill") → What type of `Quest` is this
- `quest_target` (Item if 'fetch_or_kill' == 'fetch', or Mob if 'fetch_or_kill' == 'kill') → The target of the `Quest`
- `quest_reward` (Item or None) → Reward given for completing the `Quest`
- `final_quest` (Bool) → Does completing this `Quest` end the game?

```
# Engine Instance (VuurEngine), Name (String), fetch_ot_kill (String), quest_target (Item or Mob), quest_reward (Item), final_quest (Bool)
protect_quest = vuurengine.Item(your_game_engine, "Protect Denny", "kill", chrisr_mob, lisas_dress, False)
```

All `Quest` objects have a dictionary, called `quest_info`, that stores information about the `Quest`. The values stored in the dictionary are as follows: 

- `name` (String) → The name of the `Quest`
- `fetch_ot_kill` (String, specifically "fetch" or "kill") → What type of `Quest` is this
- `quest_target` (Item if 'fetch_or_kill' == 'fetch', or Mob if 'fetch_or_kill' == 'kill') → The target of the `Quest`
- `quest_reward` (Item or None) → Reward given for completing the `Quest`
- `prereqs` (List of Quests) → The `Quest`s that need to be completed prior to getting this `Quest`
- `questgiver` (Questmaster) → The `Questmaster` who gives out the quest
- `assigned_to` (Player) → Who is doing the `Quest`
- `completed` (Bool) → Has this `Quest` been completed?
- `final_quest` (Bool) → Does completing this `Quest` end the game?

It's a good idea to let the game handle changing/setting these values on its own for a `Quest`, but if you need to adjust them for whatever reason (such as to set what is written on an `Item`) or check a value, you can use the following fuctions.

The `getInfo()` function will return a value from the `quest_info` dictionary. Just pass the name of the entry in the `quest_info` dictionary.
```
# entry_to_request (String)
getInfo(entry_to_request):
```

The `setInfo()` function will set the value for a particualr entry in the `quest_info` dictionary.
```
# entry_to_set (String), value_to_set (???)
setInfo(entry_to_set, value_to_set):
```

The `addPrereq()` function will add a prerequisite `Quest`.
```
# prereq_quest (Quest)
addPrereq(prereq_quest):
```

## Setting up a server

As mentioned, VuurEngine games **are not** multiplayer, but they can still be run on a server that allows for remote access.

The easiest way to set up a server that I've found has been to use Nmap's `netcat`-workalike, `ncat`, and Docker. Below is the script I use to _actually_ run the server; it does make some assumptions, but _assuming_ you're doing it exactly as I do, those assumptions won't be an issue.


```
#!/usr/bin/bash
while true
do
	/usr/bin/ncat -l -k -p 64132 -v -c '/usr/bin/python3 /opt/vuurengine/vegame.py /opt/vuurengine/examplegame_plaintext.vegame' && wait
done
```

That script should be saved in a directory as `server.sh` alongside a Dockerfile (named `Dockerfile`) containing the following:

```
# TO BUILD:
# 	docker build -t vuurengine_example_game .
# 
# TO RUN:
#	docker run -d -p 64132:64132 vuurengine_example_game
#
# TO CONNECT:
# 	nc $THIS_SERVER 64132

# Use AlpineLinux base image
FROM alpine

# Create working dir
WORKDIR /opt/vuurengine

# Add dependencies
RUN apk add bash
RUN apk add screen
RUN apk add python3
RUN apk add nmap-ncat
RUN apk add git

# Clone git Repo of Game
RUN git clone https://gitlab.com/cblanke2/vuurengine.git /opt/vuurengine
RUN git -C /opt/vuurengine pull

# Expose port
EXPOSE 64132

# Run the ncat server(s)
COPY ./server.sh /opt/vuurengine/server.sh
CMD /bin/bash /opt/vuurengine/server.sh
```

This creates a Docker container that contains all of the dependencies, clones this repo into the proper location, and runs the aforementioned server script. By default, the port it uses is **64132**, but this can be changed. Anything about this whole thing can be changed; I put it out under an open souce license afterall. As someone not-so-wise once said to someone I know: "The world is your oyster, but there's a bathroom inside." 

To build it, just run:

```
docker build -t vuurengine_example_game .
```

And then to run the container, run:
```
docker run -d -p 64132:64132 vuurengine_example_game
```

I'll eventually get a publicly accessible docker image up somewhere. Eventually.
